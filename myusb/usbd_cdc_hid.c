#include "usbd_cdc_hid.h"
#include "usbd_desc.h"
#include "usbd_ctlreq.h"
#include "usbd_cdc.h"
#include "usbd_hid.h"


static uint8_t  USBD_CDC_HID_Init (USBD_HandleTypeDef *pdev, uint8_t cfgidx);
static uint8_t  USBD_CDC_HID_DeInit (USBD_HandleTypeDef *pdev, uint8_t cfgidx);
static uint8_t  USBD_CDC_HID_Setup (USBD_HandleTypeDef *pdev, USBD_SetupReqTypedef *req);
static const uint8_t  *USBD_CDC_HID_GetFSConfigDescriptor (uint16_t *length);
static const uint8_t  *USBD_CDC_HID_GetDeviceQualifierDescriptor (uint16_t *length);
static uint8_t  USBD_CDC_HID_DataIn (USBD_HandleTypeDef *pdev, uint8_t epnum);
static uint8_t  USBD_CDC_HID_DataOut (USBD_HandleTypeDef *pdev, uint8_t epnum);
static uint8_t  USBD_CDC_HID_EP0_RxReady (USBD_HandleTypeDef *pdev);

USBD_ClassTypeDef USBD_CDC_HID_ClassDriver =
{
	USBD_CDC_HID_Init,
	USBD_CDC_HID_DeInit,
	USBD_CDC_HID_Setup,
	NULL, //EP0_TxSent,
	USBD_CDC_HID_EP0_RxReady,
	USBD_CDC_HID_DataIn,
	USBD_CDC_HID_DataOut,
	NULL, //USBD_CDC_HID_SOF,
	NULL, //USBD_CDC_HID_IsoINIncomplete,
	NULL, //USBD_CDC_HID_IsoOutIncomplete,
	USBD_CDC_HID_GetFSConfigDescriptor,
	USBD_CDC_HID_GetFSConfigDescriptor,
	USBD_CDC_HID_GetFSConfigDescriptor,
	USBD_CDC_HID_GetDeviceQualifierDescriptor,
};

#if defined ( __ICCARM__ ) /*!< IAR Compiler */
#pragma data_alignment=4
#endif


#define USB_CDC_HID_CONFIG_DESC_SIZ  (67U + 8 + 25U + 25U)

static const uint8_t USBD_CDC_HID_CfgDesc[] =
{
		/* Configuration Descriptor */
		0x09,                                       /* bLength: Configuration Descriptor size */
		USB_DESC_TYPE_CONFIGURATION,                /* bDescriptorType: Configuration */
		USB_CDC_HID_CONFIG_DESC_SIZ,                    /* wTotalLength:no of returned bytes */
		0x00,
		0x04,                                       /* bNumInterfaces: 2 interface */
		0x01,                                       /* bConfigurationValue: Configuration value */
		0x00,                                       /* iConfiguration: Index of string descriptor describing the configuration */
		0xC0,                                       /* bmAttributes: self powered */
		0x32,                                       /* MaxPower 0 mA */


		/******** IAD should be positioned just before the CDC interfaces ******
		  		                                 	IAD to associate the two CDC interfaces */
		0x08, /* bLength */
		0x0B, /* bDescriptorType */
		0, /* bFirstInterface */
		0x02, /* bInterfaceCount */
		0x02, /* bFunctionClass */
		0x02, /* bFunctionSubClass */
		0x01, /* bFunctionProtocol */
		0x00, /* iFunction (Index of string descriptor describing this function) */

		/* Interface Descriptor */
		0x09,                                       /* bLength: Interface Descriptor size */
		USB_DESC_TYPE_INTERFACE,                    /* bDescriptorType: Interface */
		/* Interface descriptor type */
		0x00,                                       /* bInterfaceNumber: Number of Interface */
		0x00,                                       /* bAlternateSetting: Alternate setting */
		0x01,                                       /* bNumEndpoints: One endpoints used */
		0x02,                                       /* bInterfaceClass: Communication Interface Class */
		0x02,                                       /* bInterfaceSubClass: Abstract Control Model */
		0x01,                                       /* bInterfaceProtocol: Common AT commands */
		0x00,                                       /* iInterface: */

		/* Header Functional Descriptor */
		0x05,                                       /* bLength: Endpoint Descriptor size */
		0x24,                                       /* bDescriptorType: CS_INTERFACE */
		0x00,                                       /* bDescriptorSubtype: Header Func Desc */
		0x10,                                       /* bcdCDC: spec release number */
		0x01,

		/* Call Management Functional Descriptor */
		0x05,                                       /* bFunctionLength */
		0x24,                                       /* bDescriptorType: CS_INTERFACE */
		0x01,                                       /* bDescriptorSubtype: Call Management Func Desc */
		0x00,                                       /* bmCapabilities: D0+D1 */
		0x01,                                       /* bDataInterface: 1 */

		/* ACM Functional Descriptor */
		0x04,                                       /* bFunctionLength */
		0x24,                                       /* bDescriptorType: CS_INTERFACE */
		0x02,                                       /* bDescriptorSubtype: Abstract Control Management desc */
		0x02,                                       /* bmCapabilities */

		/* Union Functional Descriptor */
		0x05,                                       /* bFunctionLength */
		0x24,                                       /* bDescriptorType: CS_INTERFACE */
		0x06,                                       /* bDescriptorSubtype: Union func desc */
		0x00,                                       /* bMasterInterface: Communication class interface */
		0x01,                                       /* bSlaveInterface0: Data Class Interface */

		/* Endpoint 2 Descriptor */
		0x07,                                       /* bLength: Endpoint Descriptor size */
		USB_DESC_TYPE_ENDPOINT,                     /* bDescriptorType: Endpoint */
		CDC_CMD_EP,                                 /* bEndpointAddress */
		0x03,                                       /* bmAttributes: Interrupt */
		LOBYTE(CDC_CMD_PACKET_SIZE),                /* wMaxPacketSize: */
		HIBYTE(CDC_CMD_PACKET_SIZE),
		CDC_FS_BINTERVAL,                           /* bInterval: */

		/* Data class interface descriptor */
		0x09,                                       /* bLength: Endpoint Descriptor size */
		USB_DESC_TYPE_INTERFACE,                    /* bDescriptorType: */
		0x01,                                       /* bInterfaceNumber: Number of Interface */
		0x00,                                       /* bAlternateSetting: Alternate setting */
		0x02,                                       /* bNumEndpoints: Two endpoints used */
		0x0A,                                       /* bInterfaceClass: CDC */
		0x00,                                       /* bInterfaceSubClass: */
		0x00,                                       /* bInterfaceProtocol: */
		0x00,                                       /* iInterface: */

		/* Endpoint OUT Descriptor */
		0x07,                                       /* bLength: Endpoint Descriptor size */
		USB_DESC_TYPE_ENDPOINT,                     /* bDescriptorType: Endpoint */
		CDC_OUT_EP,                                 /* bEndpointAddress */
		0x02,                                       /* bmAttributes: Bulk */
		LOBYTE(CDC_DATA_FS_MAX_PACKET_SIZE),        /* wMaxPacketSize: */
		HIBYTE(CDC_DATA_FS_MAX_PACKET_SIZE),
		0x00,                                       /* bInterval: ignore for Bulk transfer */

		/* Endpoint IN Descriptor */
		0x07,                                       /* bLength: Endpoint Descriptor size */
		USB_DESC_TYPE_ENDPOINT,                     /* bDescriptorType: Endpoint */
		CDC_IN_EP,                                  /* bEndpointAddress */
		0x02,                                       /* bmAttributes: Bulk */
		LOBYTE(CDC_DATA_FS_MAX_PACKET_SIZE),        /* wMaxPacketSize: */
		HIBYTE(CDC_DATA_FS_MAX_PACKET_SIZE),
		0x00,                                        /* bInterval: ignore for Bulk transfer */



		/************** Descriptor of HID interface ****************/
		0x09,                                               /* bLength: Interface Descriptor size */
		USB_DESC_TYPE_INTERFACE,                            /* bDescriptorType: Interface descriptor type */
		HID_INTERFACE_IDX,                                  /* bInterfaceNumber: Number of Interface */
		0x00,                                               /* bAlternateSetting: Alternate setting */
		0x01,                                               /* bNumEndpoints */
		0x03,                                               /* bInterfaceClass: HID */
		0x01,                                               /* bInterfaceSubClass : 1=BOOT, 0=no boot */
		0x01,                                               /* nInterfaceProtocol : 0=none, 1=keyboard, 2=mouse */
		0,                                                  /* iInterface: Index of string descriptor */

		/******************** Descriptor of HID ********************/
		0x09,                                               /* bLength: HID Descriptor size */
		HID_DESCRIPTOR_TYPE,                                /* bDescriptorType: HID */
		0x11,                                               /* bcdHID: HID Class Spec release number */
		0x01,
		0x00,                                               /* bCountryCode: Hardware target country */
		0x01,                                               /* bNumDescriptors: Number of HID class descriptors to follow */
		0x22,                                               /* bDescriptorType */
		HID_REPORT_DESC_SIZE,                         /* wItemLength: Total length of Report descriptor */
		0x00,

		/******************** HID endpoint ********************/
		0x07,                                               /* bLength: Endpoint Descriptor size */
		USB_DESC_TYPE_ENDPOINT,                             /* bDescriptorType:*/
		HID_EPIN_ADDR,                                     	 	/* bEndpointAddress: Endpoint Address (IN) */
		0x03,                                               /* bmAttributes: Interrupt endpoint */
		HID_EPIN_SIZE,                                      /* wMaxPacketSize: 4 Byte max */
		0x00,
		HID_FS_BINTERVAL,                                   /* bInterval: Polling Interval */

		/************** HID interface 2 ****************/
		0x09,                                               /* bLength: Interface Descriptor size */
		USB_DESC_TYPE_INTERFACE,                            /* bDescriptorType: Interface descriptor type */
		HID_INTERFACE_IDX + 1,                                  /* bInterfaceNumber: Number of Interface */
		0x00,                                               /* bAlternateSetting: Alternate setting */
		0x01,                                               /* bNumEndpoints */
		0x03,                                               /* bInterfaceClass: HID */
		0x01,                                               /* bInterfaceSubClass : 1=BOOT, 0=no boot */
		0x01,                                               /* nInterfaceProtocol : 0=none, 1=keyboard, 2=mouse */
		0,                                                  /* iInterface: Index of string descriptor */

		/******************** Descriptor of HID 2 ********************/
		0x09,                                               /* bLength: HID Descriptor size */
		HID_DESCRIPTOR_TYPE,                                /* bDescriptorType: HID */
		0x11,                                               /* bcdHID: HID Class Spec release number */
		0x01,
		0x00,                                               /* bCountryCode: Hardware target country */
		0x01,                                               /* bNumDescriptors: Number of HID class descriptors to follow */
		0x22,                                               /* bDescriptorType */
		HID_REPORT_DESC_SIZE,                         /* wItemLength: Total length of Report descriptor */
		0x00,

		/******************** Descriptor of HID2 endpoint ********************/
		0x07,                                               /* bLength: Endpoint Descriptor size */
		USB_DESC_TYPE_ENDPOINT,                             /* bDescriptorType:*/
		HID_EPIN_ADDR + 1,                                  /* bEndpointAddress: Endpoint Address (IN) */
		0x03,                                               /* bmAttributes: Interrupt endpoint */
		HID_EPIN_SIZE,                                      /* wMaxPacketSize: 4 Byte max */
		0x00,
		HID_FS_BINTERVAL,                                   /* bInterval: Polling Interval */
};


static uint8_t  USBD_CDC_HID_Init (USBD_HandleTypeDef *pdev,
								   uint8_t cfgidx)
{
	uint8_t	ret = USBD_CDC.Init(pdev, cfgidx);
	if(ret != 0)
		return ret;

	ret = USBD_HID_Init(0, pdev, cfgidx);
	if(ret != 0)
		return ret;

	ret = USBD_HID_Init(1, pdev, cfgidx);
	if(ret != 0)
		return ret;

	return USBD_OK;
}


static uint8_t  USBD_CDC_HID_DeInit (USBD_HandleTypeDef *pdev, uint8_t cfgidx)
{
	USBD_CDC.DeInit(pdev, cfgidx);
	USBD_HID_DeInit(0, pdev, cfgidx);
	USBD_HID_DeInit(1, pdev, cfgidx);
	return USBD_OK;
}

static uint8_t  USBD_CDC_HID_Setup (USBD_HandleTypeDef *pdev, USBD_SetupReqTypedef *req)
{
	uint8_t reqType = req->bmRequest & USB_REQ_RECIPIENT_MASK;
	if((reqType == USB_REQ_RECIPIENT_INTERFACE && req->wIndex == HID_INTERFACE_IDX) ||
		(reqType == USB_REQ_RECIPIENT_ENDPOINT && ((req->wIndex & 0x7F) == (HID_EPIN_ADDR & 0x7F))))
	{
		return USBD_HID_Setup(0, pdev, req);
	}

	if((reqType == USB_REQ_RECIPIENT_INTERFACE && req->wIndex == HID_INTERFACE_IDX + 1) ||
			(reqType == USB_REQ_RECIPIENT_ENDPOINT && ((req->wIndex & 0x7F) == ((HID_EPIN_ADDR + 1) & 0x7F))))
	{
		return USBD_HID_Setup(1, pdev, req);
	}

	return USBD_CDC.Setup(pdev, req);
}

static uint8_t  USBD_CDC_HID_DataIn (USBD_HandleTypeDef *pdev, uint8_t epnum)
{
	if(epnum == (HID_EPIN_ADDR & 0x7F))
		return USBD_HID_DataIn(0, pdev, epnum);

	if(epnum == ((HID_EPIN_ADDR + 1) & 0x7F))
		return USBD_HID_DataIn(1, pdev, epnum);

	return USBD_CDC.DataIn(pdev, epnum);
}

static uint8_t  USBD_CDC_HID_DataOut (USBD_HandleTypeDef *pdev,
									  uint8_t epnum)
{
	/*
	if(epnum == (HID_EPIN_ADDR & 0x7F))
		return USBD_HID_DataOut(0, pdev, epnum);

	if(epnum == ((HID_EPIN_ADDR+1) & 0x7F))
		return USBD_HID_DataOut(0, pdev, epnum);
*/
	return USBD_CDC.DataOut(pdev, epnum);
}

static uint8_t USBD_CDC_HID_EP0_RxReady (USBD_HandleTypeDef *pdev)
{
	return USBD_CDC.EP0_RxReady(pdev);
}

static const uint8_t  *USBD_CDC_HID_GetFSConfigDescriptor (uint16_t *length)
{
	_Static_assert(sizeof (USBD_CDC_HID_CfgDesc) == USB_CDC_HID_CONFIG_DESC_SIZ, "invalid USB_CDC_HID_CONFIG_DESC_SIZ");
	*length = sizeof (USBD_CDC_HID_CfgDesc);
	return USBD_CDC_HID_CfgDesc;
}

const uint8_t *USBD_CDC_HID_GetDeviceQualifierDescriptor (uint16_t *length)
{
	return USBD_CDC.GetDeviceQualifierDescriptor(length);
}


