#include "usbd_cdc_if.h"
#include "usbd_cdc.h"
#include "../joy5/joy5_ext.h"

#define APP_RX_DATA_SIZE  2048
#define APP_TX_DATA_SIZE  2048

uint8_t UserRxBufferFS[APP_RX_DATA_SIZE];
uint8_t UserTxBufferFS[APP_TX_DATA_SIZE];

volatile int gUserTxBufferCurPos = 0;
volatile int gUserTxBufferTransmitEndPos = 0;

uint8_t UserRxBufferFSCopy[APP_RX_DATA_SIZE];
volatile int gUserRxBufferCopyPos = 0;

void checkUserTxBufferStatus();
void checkUserRxBufferStatus();


extern USBD_HandleTypeDef hUsbDeviceFS;

static int8_t CDC_Init_FS(void);
static int8_t CDC_DeInit_FS(void);
static int8_t CDC_Control_FS(uint8_t cmd, uint8_t* pbuf, uint16_t length);
static int8_t CDC_Receive_FS(uint8_t* pbuf, uint32_t *Len);
static int8_t CDC_TransmitCplt_FS(uint8_t *pbuf, uint32_t *Len, uint8_t epnum);

USBD_CDC_ItfTypeDef USBD_Interface_fops_FS =
{
  CDC_Init_FS,
  CDC_DeInit_FS,
  CDC_Control_FS,
  CDC_Receive_FS,
  CDC_TransmitCplt_FS
};

static int8_t CDC_Init_FS(void)
{
  USBD_CDC_SetTxBuffer(&hUsbDeviceFS, UserTxBufferFS, 0);
  USBD_CDC_SetRxBuffer(&hUsbDeviceFS, UserRxBufferFS);
  return USBD_OK;
}

static int8_t CDC_DeInit_FS(void)
{
  return USBD_OK;
}

static int8_t CDC_Control_FS(uint8_t cmd, uint8_t* pbuf, uint16_t length)
{
  return USBD_OK;
}

volatile int gCantReceiveMoreData = 0;

static int8_t CDC_Receive_FS(uint8_t* Buf, uint32_t *Len)
{
	memcpy(UserRxBufferFSCopy + gUserRxBufferCopyPos, Buf, *Len);
	gUserRxBufferCopyPos += *Len;

	if (gUserRxBufferCopyPos <= APP_RX_DATA_SIZE / 2) {
		USBD_CDC_SetRxBuffer(&hUsbDeviceFS, UserRxBufferFS);
		USBD_CDC_ReceivePacket(&hUsbDeviceFS);
	} else {
		gCantReceiveMoreData = 1;
	}
	return USBD_OK;
}

uint8_t CDC_Transmit_FS(uint8_t* Buf, uint16_t Len)
{
  uint8_t result = USBD_OK;
  USBD_CDC_HandleTypeDef *hcdc = (USBD_CDC_HandleTypeDef*)hUsbDeviceFS.pClassData;
  if (hcdc->TxState != 0){
    return USBD_BUSY;
  }
  USBD_CDC_SetTxBuffer(&hUsbDeviceFS, Buf, Len);
  result = USBD_CDC_TransmitPacket(&hUsbDeviceFS);
  return result;
}

static int8_t CDC_TransmitCplt_FS(uint8_t *Buf, uint32_t *Len, uint8_t epnum)
{
	uint8_t result = USBD_OK;
	UNUSED(Buf);
	UNUSED(Len);
	UNUSED(epnum);
	memmove(UserTxBufferFS, UserTxBufferFS + gUserTxBufferTransmitEndPos, gUserTxBufferCurPos - gUserTxBufferTransmitEndPos);
	gUserTxBufferCurPos -= gUserTxBufferTransmitEndPos;
	gUserTxBufferTransmitEndPos = 0;
	checkUserTxBufferStatus();
	return result;
}


void checkUserTxBufferStatus() {
	int size = gUserTxBufferCurPos;
	if (size > 0 && CDC_Transmit_FS(UserTxBufferFS, size) == USBD_OK) {
		gUserTxBufferTransmitEndPos = size;
	}
}

Joy5ErrorCode joy5_send(const char* data, int len) {
	if (len == 0)return 0;
	while (len > APP_TX_DATA_SIZE/2) {
		joy5_send(data, APP_TX_DATA_SIZE/2);
		data += APP_TX_DATA_SIZE/2;
		len -= APP_TX_DATA_SIZE/2;
	}

	int maxLen;
	do {
		maxLen = APP_TX_DATA_SIZE - gUserTxBufferCurPos;
		if (len > maxLen) {
			HAL_Delay(0);
			continue;
		}
	} while(0);

	__disable_irq();
	memcpy(UserTxBufferFS + gUserTxBufferCurPos, data, len);
	gUserTxBufferCurPos += len;
	checkUserTxBufferStatus();
	__enable_irq();
	return 0;
}

Joy5Boolean joy5_is_send_buffer_empty() {
	USBD_CDC_HandleTypeDef *hcdc = (USBD_CDC_HandleTypeDef*)hUsbDeviceFS.pClassData;
	return hcdc->TxState == 0;
}

int joy5_receive(char* data, int data_len) {
	__disable_irq();
	int len = data_len < gUserRxBufferCopyPos ? data_len : gUserRxBufferCopyPos;
	memcpy(data, UserRxBufferFSCopy, len);
	gUserRxBufferCopyPos -= len;
	//refactor
	memmove(UserRxBufferFSCopy, UserRxBufferFSCopy + len, gUserRxBufferCopyPos);
	if (gCantReceiveMoreData && gUserRxBufferCopyPos <= APP_RX_DATA_SIZE / 2) {
		gCantReceiveMoreData = 0;
		USBD_CDC_SetRxBuffer(&hUsbDeviceFS, UserRxBufferFS);
		USBD_CDC_ReceivePacket(&hUsbDeviceFS);
	}

	__enable_irq();
	return len;
}


void joy5_delay() {
	HAL_Delay(1);
}
