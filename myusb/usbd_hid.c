#include "usbd_hid.h"
#include "usbd_ctlreq.h"

__ALIGN_BEGIN static uint8_t USBD_HID_Keyboard_Desc[USB_HID_DESC_SIZ] __ALIGN_END = {
  /* 18 */
  0x09,                                               /* bLength: HID Descriptor size */
  HID_DESCRIPTOR_TYPE,                                /* bDescriptorType: HID */
  0x11,                                               /* bcdHID: HID Class Spec release number */
  0x01,
  0x00,                                               /* bCountryCode: Hardware target country */
  0x01,                                               /* bNumDescriptors: Number of HID class descriptors to follow */
  0x22,                                               /* bDescriptorType */
  HID_REPORT_DESC_SIZE,                         /* wItemLength: Total length of Report descriptor */
  0x00,
};


__ALIGN_BEGIN static uint8_t HID_Keyboard_ReportDesc[HID_REPORT_DESC_SIZE] __ALIGN_END = {
		0x05, 0x01,                    // USAGE_PAGE (Generic Desktop)
		0x09, 0x06,                    // USAGE (Keyboard)
		0xa1, 0x01,                    // COLLECTION (Application)
		0x05, 0x07,                    //   USAGE_PAGE (Keyboard)
		0x19, 0xe0,                    //   USAGE_MINIMUM (Keyboard LeftControl)
		0x29, 0xe7,                    //   USAGE_MAXIMUM (Keyboard Right GUI)
		0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
		0x25, 0x01,                    //   LOGICAL_MAXIMUM (1)
		0x75, 0x01,                    //   REPORT_SIZE (1)
		0x95, 0x08,                    //   REPORT_COUNT (8)
		0x81, 0x02,                    //   INPUT (Data,Var,Abs)
		0x95, 0x01,                    //   REPORT_COUNT (1)
		0x75, 0x08,                    //   REPORT_SIZE (8)
		0x81, 0x03,                    //   INPUT (Cnst,Var,Abs)
		0x95, 0x05,                    //   REPORT_COUNT (5)
		0x75, 0x01,                    //   REPORT_SIZE (1)
		0x05, 0x08,                    //   USAGE_PAGE (LEDs)
		0x19, 0x01,                    //   USAGE_MINIMUM (Num Lock)
		0x29, 0x05,                    //   USAGE_MAXIMUM (Kana)
		0x91, 0x02,                    //   OUTPUT (Data,Var,Abs)
		0x95, 0x01,                    //   REPORT_COUNT (1)
		0x75, 0x03,                    //   REPORT_SIZE (3)
		0x91, 0x03,                    //   OUTPUT (Cnst,Var,Abs)
		0x95, 0x06,                    //   REPORT_COUNT (6)
		0x75, 0x08,                    //   REPORT_SIZE (8)
		0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
		0x25, 0x65,                    //   LOGICAL_MAXIMUM (101)
		0x05, 0x07,                    //   USAGE_PAGE (Keyboard)
		0x19, 0x00,                    //   USAGE_MINIMUM (Reserved (no event indicated))
		0x29, 0x65,                    //   USAGE_MAXIMUM (Keyboard Application)
		0x81, 0x00,                    //   INPUT (Data,Ary,Abs)
		0xc0                           // END_COLLECTION
};


uint8_t USBD_HID_Init(uint32_t nDevice, USBD_HandleTypeDef *pdev, uint8_t cfgidx)
{
  UNUSED(cfgidx);

  USBD_HID_HandleTypeDef *hhid;

  hhid = malloc(sizeof(USBD_HID_HandleTypeDef));
  //memset(hhid, 0, sizeof(USBD_HID_HandleTypeDef));

  if (hhid == NULL)
  {
    pdev->pClassDataHID[nDevice] = NULL;
    return (uint8_t)USBD_EMEM;
  }

  pdev->pClassDataHID[nDevice] = (void *)hhid;

  if (pdev->dev_speed == USBD_SPEED_HIGH)
  {
    pdev->ep_in[(HID_EPIN_ADDR+nDevice) & 0xFU].bInterval = HID_HS_BINTERVAL;
  }
  else   /* LOW and FULL-speed endpoints */
  {
    pdev->ep_in[(HID_EPIN_ADDR+nDevice) & 0xFU].bInterval = HID_FS_BINTERVAL;
  }

    /* Open EP IN */
  (void)USBD_LL_OpenEP(pdev, (HID_EPIN_ADDR+nDevice), USBD_EP_TYPE_INTR, HID_EPIN_SIZE);
  pdev->ep_in[(HID_EPIN_ADDR+nDevice) & 0xFU].is_used = 1U;

  hhid->state = HID_IDLE;

  return (uint8_t)USBD_OK;
}


uint8_t USBD_HID_DeInit(uint32_t nDevice, USBD_HandleTypeDef *pdev, uint8_t cfgidx)
{
  UNUSED(cfgidx);

  /* Close HID EPs */
  (void)USBD_LL_CloseEP(pdev, (HID_EPIN_ADDR+nDevice));
  pdev->ep_in[(HID_EPIN_ADDR+nDevice) & 0xFU].is_used = 0U;
  pdev->ep_in[(HID_EPIN_ADDR+nDevice) & 0xFU].bInterval = 0U;

  /* FRee allocated memory */
  if (pdev->pClassDataHID[nDevice] != NULL)
  {
    free(pdev->pClassDataHID[nDevice]);
    pdev->pClassDataHID[nDevice] = NULL;
  }

  return (uint8_t)USBD_OK;
}


uint8_t USBD_HID_Setup(uint32_t nDevice,USBD_HandleTypeDef *pdev, USBD_SetupReqTypedef *req)
{
  USBD_HID_HandleTypeDef *hhid = (USBD_HID_HandleTypeDef *)pdev->pClassDataHID[nDevice];
  USBD_StatusTypeDef ret = USBD_OK;
  uint16_t len;
  uint8_t *pbuf;
  uint16_t status_info = 0U;

  switch (req->bmRequest & USB_REQ_TYPE_MASK)
  {
  case USB_REQ_TYPE_CLASS :
    switch (req->bRequest)
    {
    case HID_REQ_SET_PROTOCOL:
      hhid->Protocol = (uint8_t)(req->wValue);
      break;

    case HID_REQ_GET_PROTOCOL:
      (void)USBD_CtlSendData(pdev, (uint8_t *)&hhid->Protocol, 1U);
      break;

    case HID_REQ_SET_IDLE:
      hhid->IdleState = (uint8_t)(req->wValue >> 8);
      break;

    case HID_REQ_GET_IDLE:
      (void)USBD_CtlSendData(pdev, (uint8_t *)&hhid->IdleState, 1U);
      break;

      /*
    case HID_REQ_SET_REPORT:
          break;
          */

    default:
      USBD_CtlError(pdev, req);
      ret = USBD_FAIL;
      break;
    }
    break;
  case USB_REQ_TYPE_STANDARD:
    switch (req->bRequest)
    {
    case USB_REQ_GET_STATUS:
      if (pdev->dev_state == USBD_STATE_CONFIGURED)
      {
        (void)USBD_CtlSendData(pdev, (uint8_t *)&status_info, 2U);
      }
      else
      {
        USBD_CtlError(pdev, req);
        ret = USBD_FAIL;
      }
      break;

    case USB_REQ_GET_DESCRIPTOR:
      if ((req->wValue >> 8) == HID_REPORT_DESC)
      {
        len = MIN(HID_REPORT_DESC_SIZE, req->wLength);
        pbuf = HID_Keyboard_ReportDesc;
      }
      else if ((req->wValue >> 8) == HID_DESCRIPTOR_TYPE)
      {
        pbuf = USBD_HID_Keyboard_Desc;
        len = MIN(USB_HID_DESC_SIZ, req->wLength);
      }
      else
      {
        USBD_CtlError(pdev, req);
        ret = USBD_FAIL;
        break;
      }
      (void)USBD_CtlSendData(pdev, pbuf, len);
      break;

    case USB_REQ_GET_INTERFACE :
      if (pdev->dev_state == USBD_STATE_CONFIGURED)
      {
        (void)USBD_CtlSendData(pdev, (uint8_t *)&hhid->AltSetting, 1U);
      }
      else
      {
        USBD_CtlError(pdev, req);
        ret = USBD_FAIL;
      }
      break;

    case USB_REQ_SET_INTERFACE:
      if (pdev->dev_state == USBD_STATE_CONFIGURED)
      {
        hhid->AltSetting = (uint8_t)(req->wValue);
      }
      else
      {
        USBD_CtlError(pdev, req);
        ret = USBD_FAIL;
      }
      break;

    case USB_REQ_CLEAR_FEATURE:
      break;

    default:
      USBD_CtlError(pdev, req);
      ret = USBD_FAIL;
      break;
    }
    break;

  default:
    USBD_CtlError(pdev, req);
    ret = USBD_FAIL;
    break;
  }

  return (uint8_t)ret;
}

uint8_t USBD_HID_SendReport(uint32_t nDevice, USBD_HandleTypeDef *pdev, uint8_t *report, uint16_t len)
{
  USBD_HID_HandleTypeDef *hhid = (USBD_HID_HandleTypeDef *)pdev->pClassDataHID[nDevice];

  if (pdev->dev_state == USBD_STATE_CONFIGURED)
  {
    if (hhid->state == HID_IDLE)
    {
      hhid->state = HID_BUSY;

      (void)USBD_LL_Transmit(pdev, (HID_EPIN_ADDR+nDevice), report, len);
    }
  }

  return (uint8_t)USBD_OK;
}

int USBD_HID_IsCanSendReport(uint32_t nDevice, USBD_HandleTypeDef *pdev) {
	USBD_HID_HandleTypeDef *hhid = (USBD_HID_HandleTypeDef *)pdev->pClassDataHID[nDevice];
	return pdev->dev_state == USBD_STATE_CONFIGURED && hhid->state == HID_IDLE;
}


uint32_t USBD_HID_GetPollingInterval(USBD_HandleTypeDef *pdev)
{
  uint32_t polling_interval;

  /* HIGH-speed endpoints */
  if (pdev->dev_speed == USBD_SPEED_HIGH)
  {
    /* Sets the data transfer polling interval for high speed transfers.
     Values between 1..16 are allowed. Values correspond to interval
     of 2 ^ (bInterval-1). This option (8 ms, corresponds to HID_HS_BINTERVAL */
    polling_interval = (((1U << (HID_HS_BINTERVAL - 1U))) / 8U);
  }
  else   /* LOW and FULL-speed endpoints */
  {
    /* Sets the data transfer polling interval for low and full
    speed transfers */
    polling_interval =  HID_FS_BINTERVAL;
  }

  return ((uint32_t)(polling_interval));
}

uint8_t USBD_HID_DataIn(uint32_t nDevice, USBD_HandleTypeDef *pdev, uint8_t epnum)
{
  UNUSED(epnum);
  /* Ensure that the FIFO is empty before a new transfer, this condition could
  be caused by  a new transfer before the end of the previous transfer */
  ((USBD_HID_HandleTypeDef *)pdev->pClassDataHID[nDevice])->state = HID_IDLE;

  return (uint8_t)USBD_OK;
}
