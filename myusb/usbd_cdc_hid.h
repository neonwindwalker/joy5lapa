#pragma once

#ifdef __cplusplus
 extern "C" {
#endif

#include  "usbd_ioreq.h"

#define HID_INTERFACE_IDX 0x2

extern USBD_ClassTypeDef  USBD_CDC_HID_ClassDriver;

#ifdef __cplusplus
}
#endif
