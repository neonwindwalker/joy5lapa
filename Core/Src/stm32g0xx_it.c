/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32g0xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32g0xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "../../myusb/usbd_hid.h"
#include "../../joy5/joy5_ext.h"
extern USBD_HandleTypeDef hUsbDeviceFS;
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
#define JOY5_KEYBOARD_EVENTS_BUFFER 5

KeyboardHIDData gKbSendBuffer[2][JOY5_KEYBOARD_EVENTS_BUFFER];
uint32_t gKbSendBufferPos[2] = {0};
uint32_t gKbNeedSendEmpty[2] = {0};
KeyboardHIDData gKbSendHIDData[2];

void send_keyboard_event(int device) {
	if (!USBD_HID_IsCanSendReport(device, &hUsbDeviceFS))return;
	if (gKbNeedSendEmpty[device]) {
		KeyboardHIDData hidup = {0};
		gKbSendHIDData[device] = hidup;
		USBD_HID_SendReport(device, &hUsbDeviceFS, &gKbSendHIDData[device], sizeof (KeyboardHIDData));
		gKbNeedSendEmpty[device] = 0;
	} else if (gKbSendBufferPos[device] > 0) {
		gKbSendHIDData[device] = gKbSendBuffer[device][0];
		USBD_HID_SendReport(device, &hUsbDeviceFS, &gKbSendHIDData[device], sizeof (KeyboardHIDData));
		memmove(gKbSendBuffer[device], gKbSendBuffer[device] + 1, sizeof (KeyboardHIDData) * (gKbSendBufferPos[device] - 1));
		gKbSendBufferPos[device] -= 1;
		gKbNeedSendEmpty[device] = 1;
	}
}

void joy5_send_keyboard_keycode(int device, int keycode, int is_alt, int is_control, int is_shift) {
	return;//FIXME
	__disable_irq();
	if (gKbSendBufferPos[device] >= JOY5_KEYBOARD_EVENTS_BUFFER)return;
	KeyboardHIDData hid = {0};
	hid.KEYCODE1 = keycode;
	if (is_shift) {
		hid.MODIFIER |= 0x02;
	}
	gKbSendBuffer[device][gKbSendBufferPos[device] ++] = hid;
	__enable_irq();
	send_keyboard_event(device);
}

void send_keyboard_events() {
	for (int device=0; device < 2; ++device) {
		send_keyboard_event(device);
	}
}

//must be div by 2
#define JOY5_J0_PIN_A_X GPIOC
#define JOY5_J0_PIN_A_N GPIO_PIN_15
#define JOY5_J0_PIN_B_X GPIOC
#define JOY5_J0_PIN_B_N GPIO_PIN_14
#define JOY5_J0_PIN_C_X GPIOC
#define JOY5_J0_PIN_C_N GPIO_PIN_13
#define JOY5_J0_PIN_D_X GPIOB
#define JOY5_J0_PIN_D_N GPIO_PIN_9
#define JOY5_J1_PIN_A_X GPIOB
#define JOY5_J1_PIN_A_N GPIO_PIN_8
#define JOY5_J1_PIN_B_X GPIOB
#define JOY5_J1_PIN_B_N GPIO_PIN_7
#define JOY5_J1_PIN_C_X GPIOB
#define JOY5_J1_PIN_C_N GPIO_PIN_6
#define JOY5_J1_PIN_D_X GPIOB
#define JOY5_J1_PIN_D_N GPIO_PIN_5
#define JOY5_HELP_PIN_X GPIOB
#define JOY5_HELP_PIN_N GPIO_PIN_4
#define JOY5_J2_PIN_A_X GPIOB
#define JOY5_J2_PIN_A_N GPIO_PIN_3
#define JOY5_J2_PIN_B_X GPIOD
#define JOY5_J2_PIN_B_N GPIO_PIN_3
#define JOY5_J2_PIN_C_X GPIOD
#define JOY5_J2_PIN_C_N GPIO_PIN_2
#define JOY5_J2_PIN_D_X GPIOD
#define JOY5_J2_PIN_D_N GPIO_PIN_1
#define JOY5_GAME_PIN_X GPIOD
#define JOY5_GAME_PIN_N GPIO_PIN_0
#define JOY5_J3_PIN_A_X GPIOA
#define JOY5_J3_PIN_A_N GPIO_PIN_10
#define JOY5_J3_PIN_B_X GPIOC
#define JOY5_J3_PIN_B_N GPIO_PIN_7
#define JOY5_J3_PIN_C_X GPIOC
#define JOY5_J3_PIN_C_N GPIO_PIN_6
#define JOY5_J3_PIN_D_X GPIOA
#define JOY5_J3_PIN_D_N GPIO_PIN_9
#define JOY5_J4_PIN_A_X GPIOB
#define JOY5_J4_PIN_A_N GPIO_PIN_15
#define JOY5_J4_PIN_B_X GPIOB
#define JOY5_J4_PIN_B_N GPIO_PIN_14
#define JOY5_J4_PIN_C_X GPIOB
#define JOY5_J4_PIN_C_N GPIO_PIN_13
#define JOY5_J4_PIN_D_X GPIOB
#define JOY5_J4_PIN_D_N GPIO_PIN_12
#define JOY5_LAYOUT_PIN_X GPIOB
#define JOY5_LAYOUT_PIN_N GPIO_PIN_11


#define JOY5_BUTTON_LIMIT 6
#define JOY5_BUTTON_HLIMIT 3

volatile int gPinHelp = 0;
volatile int gPinGame = 0;
volatile int gPinLayout = 0;
volatile uint32_t gStickPinLastSetTime[JOY5_STICKS][4] = {0};

void calc_button_pin(volatile int* pBtn, GPIO_PinState st) {
	if (st == GPIO_PIN_SET) {
		*pBtn += 1;
		if (*pBtn > JOY5_BUTTON_LIMIT)
			*pBtn = JOY5_BUTTON_LIMIT;
	} else {
		*pBtn -= 1;
		if (*pBtn < -JOY5_BUTTON_LIMIT)
			*pBtn = -JOY5_BUTTON_LIMIT;
	}
}

int joy5_get_button_help() {
	if (gPinHelp >= JOY5_BUTTON_HLIMIT)
		return JOY5_BUTTON_ON;
	if (gPinHelp <= -JOY5_BUTTON_HLIMIT)
		return JOY5_BUTTON_OFF;
	return JOY5_BUTTON_UNDEFINED;
}

int joy5_get_button_switch_layout() {
	if (gPinLayout >= JOY5_BUTTON_HLIMIT)
		return JOY5_BUTTON_ON;
	if (gPinLayout <= -JOY5_BUTTON_HLIMIT)
		return JOY5_BUTTON_OFF;
	return JOY5_BUTTON_UNDEFINED;
}

int joy5_get_button_game() {
	if (gPinGame >= JOY5_BUTTON_HLIMIT)
		return JOY5_BUTTON_ON;
	if (gPinGame <= -JOY5_BUTTON_HLIMIT)
		return JOY5_BUTTON_OFF;
	return JOY5_BUTTON_UNDEFINED;
}

void update_digital_input_pins() {
	calc_button_pin(&gPinHelp, HAL_GPIO_ReadPin(JOY5_HELP_PIN_X, JOY5_HELP_PIN_N));
	calc_button_pin(&gPinGame, HAL_GPIO_ReadPin(JOY5_GAME_PIN_X, JOY5_GAME_PIN_N));
	calc_button_pin(&gPinLayout, HAL_GPIO_ReadPin(JOY5_LAYOUT_PIN_X, JOY5_LAYOUT_PIN_N));

	uint32_t time = HAL_GetTick();

	if (HAL_GPIO_ReadPin(JOY5_J0_PIN_A_X, JOY5_J0_PIN_A_N) == GPIO_PIN_SET)
		gStickPinLastSetTime[0][0] = time;
	if (HAL_GPIO_ReadPin(JOY5_J0_PIN_B_X, JOY5_J0_PIN_B_N) == GPIO_PIN_SET)
		gStickPinLastSetTime[0][1] = time;
	if (HAL_GPIO_ReadPin(JOY5_J0_PIN_C_X, JOY5_J0_PIN_C_N) == GPIO_PIN_SET)
		gStickPinLastSetTime[0][2] = time;
	if (HAL_GPIO_ReadPin(JOY5_J0_PIN_D_X, JOY5_J0_PIN_D_N) == GPIO_PIN_SET)
		gStickPinLastSetTime[0][3] = time;

	if (HAL_GPIO_ReadPin(JOY5_J1_PIN_A_X, JOY5_J1_PIN_A_N) == GPIO_PIN_SET)
		gStickPinLastSetTime[1][0] = time;
	if (HAL_GPIO_ReadPin(JOY5_J1_PIN_B_X, JOY5_J1_PIN_B_N) == GPIO_PIN_SET)
		gStickPinLastSetTime[1][1] = time;
	if (HAL_GPIO_ReadPin(JOY5_J1_PIN_C_X, JOY5_J1_PIN_C_N) == GPIO_PIN_SET)
		gStickPinLastSetTime[1][2] = time;
	if (HAL_GPIO_ReadPin(JOY5_J1_PIN_D_X, JOY5_J1_PIN_D_N) == GPIO_PIN_SET)
		gStickPinLastSetTime[1][3] = time;

	if (HAL_GPIO_ReadPin(JOY5_J2_PIN_A_X, JOY5_J2_PIN_A_N) == GPIO_PIN_SET)
		gStickPinLastSetTime[2][0] = time;
	if (HAL_GPIO_ReadPin(JOY5_J2_PIN_B_X, JOY5_J2_PIN_B_N) == GPIO_PIN_SET)
		gStickPinLastSetTime[2][1] = time;
	if (HAL_GPIO_ReadPin(JOY5_J2_PIN_C_X, JOY5_J2_PIN_C_N) == GPIO_PIN_SET)
		gStickPinLastSetTime[2][2] = time;
	if (HAL_GPIO_ReadPin(JOY5_J2_PIN_D_X, JOY5_J2_PIN_D_N) == GPIO_PIN_SET)
		gStickPinLastSetTime[2][3] = time;

	if (HAL_GPIO_ReadPin(JOY5_J3_PIN_A_X, JOY5_J3_PIN_A_N) == GPIO_PIN_SET)
		gStickPinLastSetTime[3][0] = time;
	if (HAL_GPIO_ReadPin(JOY5_J3_PIN_B_X, JOY5_J3_PIN_B_N) == GPIO_PIN_SET)
		gStickPinLastSetTime[3][1] = time;
	if (HAL_GPIO_ReadPin(JOY5_J3_PIN_C_X, JOY5_J3_PIN_C_N) == GPIO_PIN_SET)
		gStickPinLastSetTime[3][2] = time;
	if (HAL_GPIO_ReadPin(JOY5_J3_PIN_D_X, JOY5_J3_PIN_D_N) == GPIO_PIN_SET)
		gStickPinLastSetTime[3][3] = time;

	if (HAL_GPIO_ReadPin(JOY5_J4_PIN_A_X, JOY5_J4_PIN_A_N) == GPIO_PIN_SET)
		gStickPinLastSetTime[4][0] = time;
	if (HAL_GPIO_ReadPin(JOY5_J4_PIN_B_X, JOY5_J4_PIN_B_N) == GPIO_PIN_SET)
		gStickPinLastSetTime[4][1] = time;
	if (HAL_GPIO_ReadPin(JOY5_J4_PIN_C_X, JOY5_J4_PIN_C_N) == GPIO_PIN_SET)
		gStickPinLastSetTime[4][2] = time;
	if (HAL_GPIO_ReadPin(JOY5_J4_PIN_D_X, JOY5_J4_PIN_D_N) == GPIO_PIN_SET)
		gStickPinLastSetTime[4][3] = time;
}

int joy5_get_stick_pin_lastsettime(int stick, int pin) {
	return gStickPinLastSetTime[stick][pin];
}

volatile int gLedDMAActive = 0;

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern PCD_HandleTypeDef hpcd_USB_DRD_FS;
extern DMA_HandleTypeDef hdma_tim1_ch1;
extern TIM_HandleTypeDef htim14;
/* USER CODE BEGIN EV */
extern TIM_HandleTypeDef htim1;
/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M0+ Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */
  while (1)
  {
  }
  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{
  /* USER CODE BEGIN SVC_IRQn 0 */

  /* USER CODE END SVC_IRQn 0 */
  /* USER CODE BEGIN SVC_IRQn 1 */

  /* USER CODE END SVC_IRQn 1 */
}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{
  /* USER CODE BEGIN PendSV_IRQn 0 */

  /* USER CODE END PendSV_IRQn 0 */
  /* USER CODE BEGIN PendSV_IRQn 1 */

  /* USER CODE END PendSV_IRQn 1 */
}

/**
  * @brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32G0xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32g0xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles USB, UCPD1 and UCPD2 global interrupts.
  */
void USB_UCPD1_2_IRQHandler(void)
{
  /* USER CODE BEGIN USB_UCPD1_2_IRQn 0 */

  /* USER CODE END USB_UCPD1_2_IRQn 0 */
  HAL_PCD_IRQHandler(&hpcd_USB_DRD_FS);
  /* USER CODE BEGIN USB_UCPD1_2_IRQn 1 */
  send_keyboard_events();
  /* USER CODE END USB_UCPD1_2_IRQn 1 */
}

/**
  * @brief This function handles DMA1 channel 1 interrupt.
  */
void DMA1_Channel1_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Channel1_IRQn 0 */
  	HAL_TIM_PWM_Stop_DMA(&htim1, TIM_CHANNEL_1);
  	gLedDMAActive = 0;
  /* USER CODE END DMA1_Channel1_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_tim1_ch1);
  /* USER CODE BEGIN DMA1_Channel1_IRQn 1 */

  /* USER CODE END DMA1_Channel1_IRQn 1 */
}

/**
  * @brief This function handles TIM14 global interrupt.
  */
void TIM14_IRQHandler(void)
{
  /* USER CODE BEGIN TIM14_IRQn 0 */
	update_digital_input_pins();
	send_keyboard_events();
  /* USER CODE END TIM14_IRQn 0 */
  HAL_TIM_IRQHandler(&htim14);
  /* USER CODE BEGIN TIM14_IRQn 1 */

  /* USER CODE END TIM14_IRQn 1 */
}

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

