#pragma once

#include "mir_reflections.h"

namespace MIR {

template<class t_T>
class Nullable {
private:
	TypeId typeId = 0;
protected:
	alignas(t_T) uint8_t mem[sizeof(t_T)];
public:
	typedef Nullable<t_T> Nullable_t;
	typedef t_T T_t;

	t_T& v() { return *reinterpret_cast<t_T*>(mem); }
	Reflection r() { return makeReflection(v()); }
	static TypeId tId() { return getTypeIdOf<T_t>(); }

	TypeId getTypeId()const { return typeId; }
	bool isNull()const { return typeId == 0; }
	bool isNotNull()const { return typeId != 0; }

	t_T& newV() {
		deleteV();
		typeId = tId();
		return *(new (mem) t_T());
	}

	t_T& newV(TypeId type) {
		deleteV();
		if (type == tId()) {
			newV();
			return v();
		}
		return nullptr;
	}

	void deleteV() {
		if (typeId == tId()) {
			v().~t_T();
		}
		typeId = 0;
	}

	void safeAssignV(const t_T& newVal) {
		if (isNull())newV();
		v() = newVal;
	}

	class Reflector : public IReflector {
	public:
		virtual int getPolymorphTypesCount(void* object){ return 1; }
		virtual Reflection getPolymorphType(void* object, uint32_t n) {
			switch(n) {
				case 0: return makeTypeReflection<t_T>();
			}
			return makeInvalidReflaction();
		}

		virtual TypeId getTypeId(void* object) {
			Nullable_t* u = reinterpret_cast<Nullable_t*>(object);
			return u->getTypeId();
		}

		virtual uint32_t getFieldsCount(void* object) {
			Nullable_t* u = reinterpret_cast<Nullable_t*>(object);
			if (u->getTypeId() == Nullable_t::tId())return u->r().getFieldsCount();
			return 0;
		}
		virtual ConstString getFieldName(void* object, uint32_t i) {
			Nullable_t* u = reinterpret_cast<Nullable_t*>(object);
			if (u->getTypeId() == Nullable_t::tId())return u->r().getFieldName(i);
			return ConstString();
		}
		virtual Reflection getFieldByName(void* object, ConstString name) {
			Nullable_t* u = reinterpret_cast<Nullable_t*>(object);
			if (u->getTypeId() == Nullable_t::tId())return u->r().getFieldByName(name);
			return makeInvalidReflaction();
		}
		virtual Reflection getFieldByNumber(void* object, uint32_t i) {
			Nullable_t* u = reinterpret_cast<Nullable_t*>(object);
			if (u->getTypeId() == Nullable_t::tId())return u->r().getFieldByNumber(i);
			return makeInvalidReflaction();
		}
		virtual ConstString getClassName(void* object) {
			Nullable_t* u = reinterpret_cast<Nullable_t*>(object);
			if (u->getTypeId() == Nullable_t::tId())return u->r().getClassName();
			return ConstString();
		}
		virtual EMetaType getMetaType(void* object) {
			return makeTypeReflection<T_t>().getMetaType();
		}
		virtual bool isNotNull(void* object){
			Nullable_t* u = reinterpret_cast<Nullable_t*>(object);
			return u->isNotNull();
		}
		virtual uint32_t getCapacity(void* object) {
			Nullable_t* u = reinterpret_cast<Nullable_t*>(object);
			if (u->getTypeId() == Nullable_t::tId())return u->r().getCapacity();
			return 0;
		}
		virtual bool remove(void* object, uint32_t i) {
			Nullable_t* u = reinterpret_cast<Nullable_t*>(object);
			if (u->getTypeId() == Nullable_t::tId())return u->r().getMetaType();
			return 0;
		}
		virtual bool insert(void* object, uint32_t i) {
			Nullable_t* u = reinterpret_cast<Nullable_t*>(object);
			if (u->getTypeId() == Nullable_t::tId())return u->r().insert(i);
			return 0;
		}
		virtual bool append(void* object) {
			Nullable_t* u = reinterpret_cast<Nullable_t*>(object);
			if (u->getTypeId() == Nullable_t::tId())return u->r().append();
			return 0;
		}
		virtual bool clear(void* object) {
			Nullable_t* u = reinterpret_cast<Nullable_t*>(object);
			if (u->getTypeId() == Nullable_t::tId())return u->r().clear();
			return 0;
		}
		virtual bool polyNew(void* object, ConstString type) {
			Nullable_t* u = reinterpret_cast<Nullable_t*>(object);
			if (type == makeTypeReflection<T_t>().getClassName()){ u->newV(); return true; }
			return false;
		}

		virtual void polyDelete(void* object) {
			Nullable_t* u = reinterpret_cast<Nullable_t*>(object);
			u->deleteV();
		}

		virtual bool print(void* object, IWriter& w, FormattingParams& fp) {
			Nullable_t* u = reinterpret_cast<Nullable_t*>(object);
			if (u->getTypeId() == Nullable_t::tId())return u->r().print(w, fp);
			return w.write("null");
		}

		virtual bool scan(void* object, IReader& r, FormattingParams& fp) {
			Nullable_t* u = reinterpret_cast<Nullable_t*>(object);
			if (u->isNull())u->newV();
			return u->r().scan(r, fp);
		}
	};
};


template<class t_T0, class t_T1, class t_BaseT>
class PolyUnion2 {
private:
	TypeId typeId = 0;
protected:
	union {
		alignas(t_T0) uint8_t mem0[sizeof(t_T0)];
		alignas(t_T1) uint8_t mem1[sizeof(t_T1)];
	};
public:
	typedef PolyUnion2<t_T0, t_T1, t_BaseT> PolyUnion_t;
	typedef t_T0 T0_t;
	typedef t_T1 T1_t;

	t_T0& v0() { return *reinterpret_cast<t_T0*>(mem0); }
	t_T1& v1() { return *reinterpret_cast<t_T1*>(mem1); }
	t_BaseT& base() { return *reinterpret_cast<t_BaseT*>(mem0); }

	Reflection r0() { return makeReflection(v0()); }
	Reflection r1() { return makeReflection(v1()); }


	static TypeId t0Id() { return getTypeIdOf<T0_t>(); }
	static TypeId t1Id() { return getTypeIdOf<T1_t>(); }

	TypeId getTypeId()const { return typeId; }
	bool isNull()const { return typeId == 0; }
	bool isNotNull()const { return typeId != 0; }

	t_T0& newV0() {
		deleteV();
		typeId = t0Id();
		return *(new (mem0) t_T0());
	}

	t_T1& newV1() {
		deleteV();
		typeId = t1Id();
		return *(new (mem1) t_T1());
	}

	t_BaseT& newV(TypeId type) {
		deleteV();
		if (type == t0Id()) {
			newV0();
			return base();
		} else if (type == t1Id()) {
			newV1();
			return base();
		}
		return *(t_BaseT*)nullptr;
	}

	void deleteV() {
		if (typeId == t0Id()) {
			v0().~t_T0();
		} else if (typeId == t1Id()) {
			v1().~t_T1();
		}
		typeId = 0;
	}


	class Reflector : public IReflector {
	public:
		virtual int getPolymorphTypesCount(void* object){ return 2; }
		virtual Reflection getPolymorphType(void* object, uint32_t n) {
			switch(n) {
				case 0: return makeTypeReflection<t_T0>();
				case 1: return makeTypeReflection<t_T1>();
			}
			return makeInvalidReflaction();
		}

		virtual TypeId getTypeId(void* object) {
			PolyUnion_t* u = reinterpret_cast<PolyUnion_t*>(object);
			return u->getTypeId();
		}

		virtual uint32_t getFieldsCount(void* object) {
			PolyUnion_t* u = reinterpret_cast<PolyUnion_t*>(object);
			if (u->getTypeId() == t0Id())return u->r0().getFieldsCount();
			if (u->getTypeId() == t1Id())return u->r1().getFieldsCount();
			return 0;
		}
		virtual ConstString getFieldName(void* object, uint32_t i) {
			PolyUnion_t* u = reinterpret_cast<PolyUnion_t*>(object);
			if (u->getTypeId() == PolyUnion_t::t0Id())return u->r0().getFieldName(i);
			if (u->getTypeId() == PolyUnion_t::t1Id())return u->r1().getFieldName(i);
			return ConstString();
		}
		virtual Reflection getFieldByName(void* object, ConstString name) {
			PolyUnion_t* u = reinterpret_cast<PolyUnion_t*>(object);
			if (u->getTypeId() == PolyUnion_t::t0Id())return u->r0().getFieldByName(name);
			if (u->getTypeId() == PolyUnion_t::t1Id())return u->r1().getFieldByName(name);
			return makeInvalidReflaction();
		}
		virtual Reflection getFieldByNumber(void* object, uint32_t i) {
			PolyUnion_t* u = reinterpret_cast<PolyUnion_t*>(object);
			if (u->getTypeId() == PolyUnion_t::t0Id())return u->r0().getFieldByNumber(i);
			if (u->getTypeId() == PolyUnion_t::t1Id())return u->r1().getFieldByNumber(i);
			return makeInvalidReflaction();
		}
		virtual ConstString getClassName(void* object) {
			PolyUnion_t* u = reinterpret_cast<PolyUnion_t*>(object);
			if (u->getTypeId() == PolyUnion_t::t0Id())return u->r0().getClassName();
			if (u->getTypeId() == PolyUnion_t::t1Id())return u->r1().getClassName();
			return ConstString();
		}
		virtual EMetaType getMetaType(void* object) {
			return EMetaType_Class;
		}
		virtual bool isNotNull(void* object){
			PolyUnion_t* u = reinterpret_cast<PolyUnion_t*>(object);
			return u->isNotNull();
		}
		virtual uint32_t getCapacity(void* object) {
			PolyUnion_t* u = reinterpret_cast<PolyUnion_t*>(object);
			if (u->getTypeId() == PolyUnion_t::t0Id())return u->r0().getCapacity();
			if (u->getTypeId() == PolyUnion_t::t1Id())return u->r1().getCapacity();
			return 0;
		}
		virtual bool remove(void* object, uint32_t i) {
			PolyUnion_t* u = reinterpret_cast<PolyUnion_t*>(object);
			if (u->getTypeId() == PolyUnion_t::t0Id())return u->r0().getMetaType();
			if (u->getTypeId() == PolyUnion_t::t1Id())return u->r1().getMetaType();
			return 0;
		}
		virtual bool insert(void* object, uint32_t i) {
			PolyUnion_t* u = reinterpret_cast<PolyUnion_t*>(object);
			if (u->getTypeId() == PolyUnion_t::t0Id())return u->r0().insert(i);
			if (u->getTypeId() == PolyUnion_t::t1Id())return u->r1().insert(i);
			return 0;
		}
		virtual bool append(void* object) {
			PolyUnion_t* u = reinterpret_cast<PolyUnion_t*>(object);
			if (u->getTypeId() == PolyUnion_t::t0Id())return u->r0().append();
			if (u->getTypeId() == PolyUnion_t::t1Id())return u->r1().append();
			return 0;
		}
		virtual bool clear(void* object) {
			PolyUnion_t* u = reinterpret_cast<PolyUnion_t*>(object);
			if (u->getTypeId() == PolyUnion_t::t0Id())return u->r0().clear();
			if (u->getTypeId() == PolyUnion_t::t1Id())return u->r1().clear();
			return 0;
		}
		virtual bool polyNew(void* object, ConstString type) {
			PolyUnion_t* u = reinterpret_cast<PolyUnion_t*>(object);
			if (type == makeTypeReflection<T0_t>().getClassName()){ u->newV0(); return true; }
			if (type == makeTypeReflection<T1_t>().getClassName()){ u->newV1(); return true; }
			return false;
		}
		virtual void polyDelete(void* object) {
			PolyUnion_t* u = reinterpret_cast<PolyUnion_t*>(object);
			u->deleteV();
		}
	};
};

}
