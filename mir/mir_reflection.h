#pragma once

#include "mir_base.h"
#include "mir_string.h"

namespace MIR {

class Reflection;
typedef int TypeId;

class IReflector {
public:
	virtual uint32_t getFieldsCount(void* object)=0;
	virtual ConstString getFieldName(void* object, uint32_t i)=0;
	virtual Reflection getFieldByName(void* object, ConstString name)=0;
	virtual Reflection getFieldByNumber(void* object, uint32_t i)=0;
	virtual ConstString getClassName(void* object)=0;
	virtual uint32_t getCapacity(void* object){ return getFieldsCount(object); }
	virtual EMetaType getMetaType(void* object) { return EMetaType_Class; }
	virtual bool isNotNull(void* object){ return object != nullptr; }
	virtual bool remove(void* object, uint32_t i){ return false; }
	virtual bool insert(void* object, uint32_t i){ return false; }
	virtual bool append(void* object){ return false; }
	virtual bool clear(void* object){ return false; }
	virtual bool print(void* object, IWriter& w, FormattingParams& fp);
	virtual bool scan(void* object, IReader& r, FormattingParams& fp);
	virtual int getPolymorphTypesCount(void* object){ return 0; }
	virtual Reflection getPolymorphType(void* object, uint32_t n);
	virtual bool polyNew(void* object, ConstString type){ return false; }
	virtual void polyDelete(void* object){}
	virtual TypeId getTypeId(void* object)=0;
	virtual uint32_t getBaseClassesCount(void* object) { return 0; }
	virtual Reflection getBaseClass(void* object, uint32_t i);
};

class InvalidReflection: public IReflector {
	virtual uint32_t getFieldsCount(void* object){ return 0; }
	virtual ConstString getFieldName(void* object, uint32_t i){ return ConstString(); }
	virtual Reflection getFieldByName(void* object, ConstString name);
	virtual Reflection getFieldByNumber(void* object, uint32_t i);
	virtual ConstString getClassName(void* object) { return ConstString(); }
	virtual uint32_t getCapacity(void* object){ return 0; }
	virtual EMetaType getMetaType(void* object) { return EMetaType_Unknown; }
	virtual bool isNotNull(void* object) { return false; }
	virtual bool remove(void* object, uint32_t i){ return false; }
	virtual bool insert(void* object, uint32_t i){ return false; }
	virtual bool append(void* object){ return false; }
	virtual bool clear(void* object){ return false; }
	virtual bool print(void* object, IWriter& w, FormattingParams& fp){ return false; }
	virtual bool scan(void* object, IReader& r, FormattingParams& fp){ return false; }
	virtual TypeId getTypeId(void* object) { return 0; }
};

struct Reflection {
	void* object_ptr;
	void* reflector_mem;

	inline Reflection() {
		object_ptr = nullptr;
		new(&r()) InvalidReflection();
	}

	inline bool isValid() { return r().getMetaType(object_ptr) != EMetaType_Unknown; }
	inline bool hasObject() { return object_ptr != nullptr; }
	inline bool isNotNull() { return r().isNotNull(object_ptr); }
	inline IReflector& r() {return *reinterpret_cast<IReflector*>(&reflector_mem); }

	inline uint32_t getFieldsCount(){ return r().getFieldsCount(object_ptr); }
	inline ConstString getFieldName(uint32_t i){ return r().getFieldName(object_ptr, i); }
	inline Reflection getFieldByName(ConstString name){ return r().getFieldByName(object_ptr, name); }
	inline Reflection getFieldByNumber(uint32_t i){ return r().getFieldByNumber(object_ptr, i); }
	inline ConstString getClassName(){ return r().getClassName(object_ptr); }
	inline uint32_t getCapacity(){ return r().getCapacity(object_ptr); }
	inline EMetaType getMetaType(){ return r().getMetaType(object_ptr); }
	inline bool remove(uint32_t i){ return r().remove(object_ptr, i); }
	inline bool insert(uint32_t i){ return r().insert(object_ptr, i); }
	inline bool append(){ return r().append(object_ptr); }
	inline bool clear(){ return r().clear(object_ptr); }
	inline bool print(IWriter& writer, FormattingParams& fp){ return r().print(object_ptr, writer, fp); }
	inline bool scan(IReader& reader, FormattingParams& fp){ return r().scan(object_ptr, reader, fp); }
	inline int getPolymorphTypesCount(){ return r().getPolymorphTypesCount(object_ptr); }
	inline Reflection getPolymorphType(uint32_t n){ return r().getPolymorphType(object_ptr, n); }
	inline bool polyNew(ConstString type){ return r().polyNew(object_ptr, type); }
	inline void polyDelete(){ r().polyDelete(object_ptr); }
	inline TypeId getTypeId() { return r().getTypeId(object_ptr); }
	inline uint32_t getBaseClassesCount(void* object) { return getBaseClassesCount(object_ptr); }
	inline Reflection getBaseClass(void* object, uint32_t i) { return getBaseClass(object_ptr, i); }

	inline bool isArray() {
		EMetaType t = getMetaType();
		return t == EMetaType_FixedArray || t == EMetaType_LimitedArray;
	}

	Reflection getByPath(ConstString path);
};

static inline Reflection makeInvalidReflaction() {
	static_assert(sizeof(InvalidReflection) <= sizeof(void*));
	Reflection r;
	r.object_ptr = nullptr;
	new (&r.reflector_mem) InvalidReflection();
	return r;
}

extern int gTypeIdSpinner;

template<typename T>
TypeId getTypeIdOf(TypeId fixedId = 0) {
	static TypeId gTypeId = 0;
	if (gTypeId != 0)return gTypeId;
	if (fixedId == 0) {
		gTypeId = gTypeIdSpinner--;
	} else {
		gTypeId = fixedId;
	}
	return gTypeId;
}

template<typename T>
void registerTypeIdOf(TypeId fixedId) {
	if (getTypeIdOf<T>(fixedId) != fixedId)abort();
}

}

