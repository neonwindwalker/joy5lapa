#include "mir.h"

namespace MIR {

bool console_cmd_ls_exec(IWriter& writer, Reflection rootReflection, StringBuff<256>& workDir, int argc, const char* const* argv) {
	Reflection r = rootReflection.getByPath(workDir);
	if (argc > 0)
		r = r.getByPath(argv[0]);
	if (!r.isValid())return false;
	EMetaType mtype = r.getMetaType();
	if (mtype == EMetaType_FixedArray) {
		ValueFormatStringBuff buff;
		buff.print("[%d]", r.getFieldsCount());
		if (!writer.write(buff))return false;
		if (!writer.writeLn())return false;
	} else if (mtype == EMetaType_LimitedArray) {
		ValueFormatStringBuff buff;
		buff.print("[%d]", r.getFieldsCount());
		if (!writer.write(buff))return false;
		if (!writer.writeLn())return false;
	} else {
		for (int i=0; i < r.getFieldsCount(); ++i) {
			if (!writer.write(r.getFieldName(i)))return false;
			if (!writer.writeLn())return false;
		}
	}
	return true;
}

bool console_cmd_cd_exec(Reflection rootReflection, StringBuff<256>& workDir, int argc, const char* const* argv) {
	if (argc < 1) {
		workDir = "/";
		return true;
	}
	ConstString path = ConstString(argv[0]).trimSpaces();
	while(path.endsWith("/"))
		path = path.trimEnd(1);
	if (path == "..") {
		const char* slash = workDir.proxy().rfind('/');
		if (slash == nullptr)return false;
		if (slash == workDir.begin())return true;
		workDir.resize(slash - workDir.begin());
		return true;
	}
	if (path.startsWith("/")) {
		Reflection r = rootReflection.getByPath(path);
		if (!r.isValid())return false;
		workDir = path;
		return true;
	}

	Reflection r = rootReflection.getByPath(workDir).getByPath(path);
	if (!r.isValid())return false;
	if (!workDir.proxy().endsWith('/')) {
		if (!workDir.append('/'))return false;
	}
	return workDir.append(path);
}

bool console_cmd_print_exec(IWriter& writer, Reflection rootReflection, StringBuff<256>& workDir, FormattingParams formattingParams, int argc, const char* const* argv) {
	Reflection r = rootReflection.getByPath(workDir);
	if (argc > 0)
		r = r.getByPath(argv[0]);
	if (!r.isValid()) return false;
	r.print(writer, formattingParams);
	return true;
}

bool console_cmd_set_exec(Reflection rootReflection, StringBuff<256>& workDir, int argc, const char* const* argv) {
	Reflection rootR = rootReflection.getByPath(workDir);
	if (!rootR.isValid()) return false;
	for (int nPar = 0; nPar * 2 + 1 < argc; ++nPar) {
		Reflection r = rootR.getByPath(argv[nPar*2 + 0]);
		if (!r.isValid()) return false;
		ConstStringReader reader(argv[nPar*2 + 1]);
		FormattingParams fp;
		if (!r.scan(reader, fp)) return false;
	}
	return true;
}

bool console_cmd_scan_exec(IReader& reader, Reflection rootReflection, StringBuff<256>& workDir, FormattingParams formattingParams, int argc, const char* const* argv) {
	Reflection r = rootReflection.getByPath(workDir);
	if (argc > 0)
		r = r.getByPath(argv[0]);
	if (!r.isValid()) return false;
	return r.scan(reader, formattingParams);
}

}
