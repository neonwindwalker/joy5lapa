#pragma once

#include "mir_reflection.h"

#define MICRO_DeclareData16(_classname, _base_class_code, _T0, _v0, _T1, _v1, _T2, _v2, _T3, _v3, _T4, _v4, _T5, _v5, _T6, _v6, _T7, _v7, _T8, _v8, _T9, _v9, _T10, _v10, _T11, _v11, _T12, _v12, _T13, _v13, _T14, _v14, _T15, _v15) \
class _classname _base_class_code { public: \
	_T0 _v0; \
	_T1 _v1; \
	_T2 _v2; \
	_T3 _v3; \
	_T4 _v4; \
	_T5 _v5; \
	_T6 _v6; \
	_T7 _v7; \
	_T8 _v8; \
	_T9 _v9; \
	_T10 _v10; \
	_T11 _v11; \
	_T12 _v12; \
	_T13 _v13; \
	_T14 _v14; \
	_T15 _v15; \
	class Reflector : public MIR::IReflector { \
		public: \
			virtual MIR::Reflection getFieldByName(void* object, MIR::ConstString name) { \
				_classname* obj = (_classname*)object; \
				if (MIR_ConstString(#_v0) ==  name) { \
					return MIR::makeReflection(obj->_v0); \
				} \
				else if (MIR_ConstString(#_v1) ==  name) { \
					return MIR::makeReflection(obj->_v1); \
				} \
				else if (MIR_ConstString(#_v2) ==  name) { \
					return MIR::makeReflection(obj->_v2); \
				} \
				else if (MIR_ConstString(#_v3) ==  name) { \
					return MIR::makeReflection(obj->_v3); \
				} \
				else if (MIR_ConstString(#_v4) ==  name) { \
					return MIR::makeReflection(obj->_v4); \
				} \
				else if (MIR_ConstString(#_v5) ==  name) { \
					return MIR::makeReflection(obj->_v5); \
				} \
				else if (MIR_ConstString(#_v6) ==  name) { \
					return MIR::makeReflection(obj->_v6); \
				} \
				else if (MIR_ConstString(#_v7) ==  name) { \
					return MIR::makeReflection(obj->_v7); \
				} \
				else if (MIR_ConstString(#_v8) ==  name) { \
					return MIR::makeReflection(obj->_v8); \
				} \
				else if (MIR_ConstString(#_v9) ==  name) { \
					return MIR::makeReflection(obj->_v9); \
				} \
				else if (MIR_ConstString(#_v10) ==  name) { \
					return MIR::makeReflection(obj->_v10); \
				} \
				else if (MIR_ConstString(#_v11) ==  name) { \
					return MIR::makeReflection(obj->_v11); \
				} \
				else if (MIR_ConstString(#_v12) ==  name) { \
					return MIR::makeReflection(obj->_v12); \
				} \
				else if (MIR_ConstString(#_v13) ==  name) { \
					return MIR::makeReflection(obj->_v13); \
				} \
				else if (MIR_ConstString(#_v14) ==  name) { \
					return MIR::makeReflection(obj->_v14); \
				} \
				else if (MIR_ConstString(#_v15) ==  name) { \
					return MIR::makeReflection(obj->_v15); \
				} \
				return MIR::Reflection(); \
			} \
			virtual uint32_t getFieldsCount(void* object) { return 16; } \
			virtual MIR::Reflection getFieldByNumber(void* object, uint32_t i) { \
				_classname* obj = (_classname*)object;  \
				switch(i) { \
					case 0: return MIR::makeReflection(obj->_v0); \
					case 1: return MIR::makeReflection(obj->_v1); \
					case 2: return MIR::makeReflection(obj->_v2); \
					case 3: return MIR::makeReflection(obj->_v3); \
					case 4: return MIR::makeReflection(obj->_v4); \
					case 5: return MIR::makeReflection(obj->_v5); \
					case 6: return MIR::makeReflection(obj->_v6); \
					case 7: return MIR::makeReflection(obj->_v7); \
					case 8: return MIR::makeReflection(obj->_v8); \
					case 9: return MIR::makeReflection(obj->_v9); \
					case 10: return MIR::makeReflection(obj->_v10); \
					case 11: return MIR::makeReflection(obj->_v11); \
					case 12: return MIR::makeReflection(obj->_v12); \
					case 13: return MIR::makeReflection(obj->_v13); \
					case 14: return MIR::makeReflection(obj->_v14); \
					case 15: return MIR::makeReflection(obj->_v15); \
				} \
				return MIR::Reflection(); \
			} \
			virtual MIR::ConstString getFieldName(void* object, uint32_t i) { \
				switch (i) { \
					case 0: return MIR_ConstString(#_v0); \
					case 1: return MIR_ConstString(#_v1); \
					case 2: return MIR_ConstString(#_v2); \
					case 3: return MIR_ConstString(#_v3); \
					case 4: return MIR_ConstString(#_v4); \
					case 5: return MIR_ConstString(#_v5); \
					case 6: return MIR_ConstString(#_v6); \
					case 7: return MIR_ConstString(#_v7); \
					case 8: return MIR_ConstString(#_v8); \
					case 9: return MIR_ConstString(#_v9); \
					case 10: return MIR_ConstString(#_v10); \
					case 11: return MIR_ConstString(#_v11); \
					case 12: return MIR_ConstString(#_v12); \
					case 13: return MIR_ConstString(#_v13); \
					case 14: return MIR_ConstString(#_v14); \
					case 15: return MIR_ConstString(#_v15); \
				} \
				return MIR::ConstString(); \
			} \
			virtual MIR::ConstString getClassName(void* object) { return MIR_ConstString(#_classname); } \
			virtual MIR::TypeId getTypeId(void* object) { return MIR::getTypeIdOf< _classname >(); } \
		}; \
		_classname() {

#define MICRO_DeclareData15(_classname, _base_class_code, _T0, _v0, _T1, _v1, _T2, _v2, _T3, _v3, _T4, _v4, _T5, _v5, _T6, _v6, _T7, _v7, _T8, _v8, _T9, _v9, _T10, _v10, _T11, _v11, _T12, _v12, _T13, _v13, _T14, _v14) \
class _classname _base_class_code { public: \
	_T0 _v0; \
	_T1 _v1; \
	_T2 _v2; \
	_T3 _v3; \
	_T4 _v4; \
	_T5 _v5; \
	_T6 _v6; \
	_T7 _v7; \
	_T8 _v8; \
	_T9 _v9; \
	_T10 _v10; \
	_T11 _v11; \
	_T12 _v12; \
	_T13 _v13; \
	_T14 _v14; \
	class Reflector : public MIR::IReflector { \
		public: \
			virtual MIR::Reflection getFieldByName(void* object, MIR::ConstString name) { \
				_classname* obj = (_classname*)object; \
				if (MIR_ConstString(#_v0) ==  name) { \
					return MIR::makeReflection(obj->_v0); \
				} \
				else if (MIR_ConstString(#_v1) ==  name) { \
					return MIR::makeReflection(obj->_v1); \
				} \
				else if (MIR_ConstString(#_v2) ==  name) { \
					return MIR::makeReflection(obj->_v2); \
				} \
				else if (MIR_ConstString(#_v3) ==  name) { \
					return MIR::makeReflection(obj->_v3); \
				} \
				else if (MIR_ConstString(#_v4) ==  name) { \
					return MIR::makeReflection(obj->_v4); \
				} \
				else if (MIR_ConstString(#_v5) ==  name) { \
					return MIR::makeReflection(obj->_v5); \
				} \
				else if (MIR_ConstString(#_v6) ==  name) { \
					return MIR::makeReflection(obj->_v6); \
				} \
				else if (MIR_ConstString(#_v7) ==  name) { \
					return MIR::makeReflection(obj->_v7); \
				} \
				else if (MIR_ConstString(#_v8) ==  name) { \
					return MIR::makeReflection(obj->_v8); \
				} \
				else if (MIR_ConstString(#_v9) ==  name) { \
					return MIR::makeReflection(obj->_v9); \
				} \
				else if (MIR_ConstString(#_v10) ==  name) { \
					return MIR::makeReflection(obj->_v10); \
				} \
				else if (MIR_ConstString(#_v11) ==  name) { \
					return MIR::makeReflection(obj->_v11); \
				} \
				else if (MIR_ConstString(#_v12) ==  name) { \
					return MIR::makeReflection(obj->_v12); \
				} \
				else if (MIR_ConstString(#_v13) ==  name) { \
					return MIR::makeReflection(obj->_v13); \
				} \
				else if (MIR_ConstString(#_v14) ==  name) { \
					return MIR::makeReflection(obj->_v14); \
				} \
				return MIR::Reflection(); \
			} \
			virtual uint32_t getFieldsCount(void* object) { return 15; } \
			virtual MIR::Reflection getFieldByNumber(void* object, uint32_t i) { \
				_classname* obj = (_classname*)object;  \
				switch(i) { \
					case 0: return MIR::makeReflection(obj->_v0); \
					case 1: return MIR::makeReflection(obj->_v1); \
					case 2: return MIR::makeReflection(obj->_v2); \
					case 3: return MIR::makeReflection(obj->_v3); \
					case 4: return MIR::makeReflection(obj->_v4); \
					case 5: return MIR::makeReflection(obj->_v5); \
					case 6: return MIR::makeReflection(obj->_v6); \
					case 7: return MIR::makeReflection(obj->_v7); \
					case 8: return MIR::makeReflection(obj->_v8); \
					case 9: return MIR::makeReflection(obj->_v9); \
					case 10: return MIR::makeReflection(obj->_v10); \
					case 11: return MIR::makeReflection(obj->_v11); \
					case 12: return MIR::makeReflection(obj->_v12); \
					case 13: return MIR::makeReflection(obj->_v13); \
					case 14: return MIR::makeReflection(obj->_v14); \
				} \
				return MIR::Reflection(); \
			} \
			virtual MIR::ConstString getFieldName(void* object, uint32_t i) { \
				switch (i) { \
					case 0: return MIR_ConstString(#_v0); \
					case 1: return MIR_ConstString(#_v1); \
					case 2: return MIR_ConstString(#_v2); \
					case 3: return MIR_ConstString(#_v3); \
					case 4: return MIR_ConstString(#_v4); \
					case 5: return MIR_ConstString(#_v5); \
					case 6: return MIR_ConstString(#_v6); \
					case 7: return MIR_ConstString(#_v7); \
					case 8: return MIR_ConstString(#_v8); \
					case 9: return MIR_ConstString(#_v9); \
					case 10: return MIR_ConstString(#_v10); \
					case 11: return MIR_ConstString(#_v11); \
					case 12: return MIR_ConstString(#_v12); \
					case 13: return MIR_ConstString(#_v13); \
					case 14: return MIR_ConstString(#_v14); \
				} \
				return MIR::ConstString(); \
			} \
			virtual MIR::ConstString getClassName(void* object) { return MIR_ConstString(#_classname); } \
			virtual MIR::TypeId getTypeId(void* object) { return MIR::getTypeIdOf< _classname >(); } \
		}; \
		_classname() {

#define MICRO_DeclareData14(_classname, _base_class_code, _T0, _v0, _T1, _v1, _T2, _v2, _T3, _v3, _T4, _v4, _T5, _v5, _T6, _v6, _T7, _v7, _T8, _v8, _T9, _v9, _T10, _v10, _T11, _v11, _T12, _v12, _T13, _v13) \
class _classname _base_class_code { public: \
	_T0 _v0; \
	_T1 _v1; \
	_T2 _v2; \
	_T3 _v3; \
	_T4 _v4; \
	_T5 _v5; \
	_T6 _v6; \
	_T7 _v7; \
	_T8 _v8; \
	_T9 _v9; \
	_T10 _v10; \
	_T11 _v11; \
	_T12 _v12; \
	_T13 _v13; \
	class Reflector : public MIR::IReflector { \
		public: \
			virtual MIR::Reflection getFieldByName(void* object, MIR::ConstString name) { \
				_classname* obj = (_classname*)object; \
				if (MIR_ConstString(#_v0) ==  name) { \
					return MIR::makeReflection(obj->_v0); \
				} \
				else if (MIR_ConstString(#_v1) ==  name) { \
					return MIR::makeReflection(obj->_v1); \
				} \
				else if (MIR_ConstString(#_v2) ==  name) { \
					return MIR::makeReflection(obj->_v2); \
				} \
				else if (MIR_ConstString(#_v3) ==  name) { \
					return MIR::makeReflection(obj->_v3); \
				} \
				else if (MIR_ConstString(#_v4) ==  name) { \
					return MIR::makeReflection(obj->_v4); \
				} \
				else if (MIR_ConstString(#_v5) ==  name) { \
					return MIR::makeReflection(obj->_v5); \
				} \
				else if (MIR_ConstString(#_v6) ==  name) { \
					return MIR::makeReflection(obj->_v6); \
				} \
				else if (MIR_ConstString(#_v7) ==  name) { \
					return MIR::makeReflection(obj->_v7); \
				} \
				else if (MIR_ConstString(#_v8) ==  name) { \
					return MIR::makeReflection(obj->_v8); \
				} \
				else if (MIR_ConstString(#_v9) ==  name) { \
					return MIR::makeReflection(obj->_v9); \
				} \
				else if (MIR_ConstString(#_v10) ==  name) { \
					return MIR::makeReflection(obj->_v10); \
				} \
				else if (MIR_ConstString(#_v11) ==  name) { \
					return MIR::makeReflection(obj->_v11); \
				} \
				else if (MIR_ConstString(#_v12) ==  name) { \
					return MIR::makeReflection(obj->_v12); \
				} \
				else if (MIR_ConstString(#_v13) ==  name) { \
					return MIR::makeReflection(obj->_v13); \
				} \
				return MIR::Reflection(); \
			} \
			virtual uint32_t getFieldsCount(void* object) { return 14; } \
			virtual MIR::Reflection getFieldByNumber(void* object, uint32_t i) { \
				_classname* obj = (_classname*)object;  \
				switch(i) { \
					case 0: return MIR::makeReflection(obj->_v0); \
					case 1: return MIR::makeReflection(obj->_v1); \
					case 2: return MIR::makeReflection(obj->_v2); \
					case 3: return MIR::makeReflection(obj->_v3); \
					case 4: return MIR::makeReflection(obj->_v4); \
					case 5: return MIR::makeReflection(obj->_v5); \
					case 6: return MIR::makeReflection(obj->_v6); \
					case 7: return MIR::makeReflection(obj->_v7); \
					case 8: return MIR::makeReflection(obj->_v8); \
					case 9: return MIR::makeReflection(obj->_v9); \
					case 10: return MIR::makeReflection(obj->_v10); \
					case 11: return MIR::makeReflection(obj->_v11); \
					case 12: return MIR::makeReflection(obj->_v12); \
					case 13: return MIR::makeReflection(obj->_v13); \
				} \
				return MIR::Reflection(); \
			} \
			virtual MIR::ConstString getFieldName(void* object, uint32_t i) { \
				switch (i) { \
					case 0: return MIR_ConstString(#_v0); \
					case 1: return MIR_ConstString(#_v1); \
					case 2: return MIR_ConstString(#_v2); \
					case 3: return MIR_ConstString(#_v3); \
					case 4: return MIR_ConstString(#_v4); \
					case 5: return MIR_ConstString(#_v5); \
					case 6: return MIR_ConstString(#_v6); \
					case 7: return MIR_ConstString(#_v7); \
					case 8: return MIR_ConstString(#_v8); \
					case 9: return MIR_ConstString(#_v9); \
					case 10: return MIR_ConstString(#_v10); \
					case 11: return MIR_ConstString(#_v11); \
					case 12: return MIR_ConstString(#_v12); \
					case 13: return MIR_ConstString(#_v13); \
				} \
				return MIR::ConstString(); \
			} \
			virtual MIR::ConstString getClassName(void* object) { return MIR_ConstString(#_classname); } \
			virtual MIR::TypeId getTypeId(void* object) { return MIR::getTypeIdOf< _classname >(); } \
		}; \
		_classname() {


#define MICRO_DeclareData13(_classname, _base_class_code, _T0, _v0, _T1, _v1, _T2, _v2, _T3, _v3, _T4, _v4, _T5, _v5, _T6, _v6, _T7, _v7, _T8, _v8, _T9, _v9, _T10, _v10, _T11, _v11, _T12, _v12) \
class _classname _base_class_code { public: \
	_T0 _v0; \
	_T1 _v1; \
	_T2 _v2; \
	_T3 _v3; \
	_T4 _v4; \
	_T5 _v5; \
	_T6 _v6; \
	_T7 _v7; \
	_T8 _v8; \
	_T9 _v9; \
	_T10 _v10; \
	_T11 _v11; \
	_T12 _v12; \
	class Reflector : public MIR::IReflector { \
		public: \
			virtual MIR::Reflection getFieldByName(void* object, MIR::ConstString name) { \
				_classname* obj = (_classname*)object; \
				if (MIR_ConstString(#_v0) ==  name) { \
					return MIR::makeReflection(obj->_v0); \
				} \
				else if (MIR_ConstString(#_v1) ==  name) { \
					return MIR::makeReflection(obj->_v1); \
				} \
				else if (MIR_ConstString(#_v2) ==  name) { \
					return MIR::makeReflection(obj->_v2); \
				} \
				else if (MIR_ConstString(#_v3) ==  name) { \
					return MIR::makeReflection(obj->_v3); \
				} \
				else if (MIR_ConstString(#_v4) ==  name) { \
					return MIR::makeReflection(obj->_v4); \
				} \
				else if (MIR_ConstString(#_v5) ==  name) { \
					return MIR::makeReflection(obj->_v5); \
				} \
				else if (MIR_ConstString(#_v6) ==  name) { \
					return MIR::makeReflection(obj->_v6); \
				} \
				else if (MIR_ConstString(#_v7) ==  name) { \
					return MIR::makeReflection(obj->_v7); \
				} \
				else if (MIR_ConstString(#_v8) ==  name) { \
					return MIR::makeReflection(obj->_v8); \
				} \
				else if (MIR_ConstString(#_v9) ==  name) { \
					return MIR::makeReflection(obj->_v9); \
				} \
				else if (MIR_ConstString(#_v10) ==  name) { \
					return MIR::makeReflection(obj->_v10); \
				} \
				else if (MIR_ConstString(#_v11) ==  name) { \
					return MIR::makeReflection(obj->_v11); \
				} \
				else if (MIR_ConstString(#_v12) ==  name) { \
					return MIR::makeReflection(obj->_v12); \
				} \
				return MIR::Reflection(); \
			} \
			virtual uint32_t getFieldsCount(void* object) { return 13; } \
			virtual MIR::Reflection getFieldByNumber(void* object, uint32_t i) { \
				_classname* obj = (_classname*)object;  \
				switch(i) { \
					case 0: return MIR::makeReflection(obj->_v0); \
					case 1: return MIR::makeReflection(obj->_v1); \
					case 2: return MIR::makeReflection(obj->_v2); \
					case 3: return MIR::makeReflection(obj->_v3); \
					case 4: return MIR::makeReflection(obj->_v4); \
					case 5: return MIR::makeReflection(obj->_v5); \
					case 6: return MIR::makeReflection(obj->_v6); \
					case 7: return MIR::makeReflection(obj->_v7); \
					case 8: return MIR::makeReflection(obj->_v8); \
					case 9: return MIR::makeReflection(obj->_v9); \
					case 10: return MIR::makeReflection(obj->_v10); \
					case 11: return MIR::makeReflection(obj->_v11); \
					case 12: return MIR::makeReflection(obj->_v12); \
				} \
				return MIR::Reflection(); \
			} \
			virtual MIR::ConstString getFieldName(void* object, uint32_t i) { \
				switch (i) { \
					case 0: return MIR_ConstString(#_v0); \
					case 1: return MIR_ConstString(#_v1); \
					case 2: return MIR_ConstString(#_v2); \
					case 3: return MIR_ConstString(#_v3); \
					case 4: return MIR_ConstString(#_v4); \
					case 5: return MIR_ConstString(#_v5); \
					case 6: return MIR_ConstString(#_v6); \
					case 7: return MIR_ConstString(#_v7); \
					case 8: return MIR_ConstString(#_v8); \
					case 9: return MIR_ConstString(#_v9); \
					case 10: return MIR_ConstString(#_v10); \
					case 11: return MIR_ConstString(#_v11); \
					case 12: return MIR_ConstString(#_v12); \
				} \
				return MIR::ConstString(); \
			} \
			virtual MIR::ConstString getClassName(void* object) { return MIR_ConstString(#_classname); } \
			virtual MIR::TypeId getTypeId(void* object) { return MIR::getTypeIdOf< _classname >(); } \
		}; \
		_classname() {

#define MICRO_DeclareData12(_classname, _base_class_code, _T0, _v0, _T1, _v1, _T2, _v2, _T3, _v3, _T4, _v4, _T5, _v5, _T6, _v6, _T7, _v7, _T8, _v8, _T9, _v9, _T10, _v10, _T11, _v11) \
class _classname _base_class_code { public: \
	_T0 _v0; \
	_T1 _v1; \
	_T2 _v2; \
	_T3 _v3; \
	_T4 _v4; \
	_T5 _v5; \
	_T6 _v6; \
	_T7 _v7; \
	_T8 _v8; \
	_T9 _v9; \
	_T10 _v10; \
	_T11 _v11; \
	class Reflector : public MIR::IReflector { \
		public: \
			virtual MIR::Reflection getFieldByName(void* object, MIR::ConstString name) { \
				_classname* obj = (_classname*)object; \
				if (MIR_ConstString(#_v0) ==  name) { \
					return MIR::makeReflection(obj->_v0); \
				} \
				else if (MIR_ConstString(#_v1) ==  name) { \
					return MIR::makeReflection(obj->_v1); \
				} \
				else if (MIR_ConstString(#_v2) ==  name) { \
					return MIR::makeReflection(obj->_v2); \
				} \
				else if (MIR_ConstString(#_v3) ==  name) { \
					return MIR::makeReflection(obj->_v3); \
				} \
				else if (MIR_ConstString(#_v4) ==  name) { \
					return MIR::makeReflection(obj->_v4); \
				} \
				else if (MIR_ConstString(#_v5) ==  name) { \
					return MIR::makeReflection(obj->_v5); \
				} \
				else if (MIR_ConstString(#_v6) ==  name) { \
					return MIR::makeReflection(obj->_v6); \
				} \
				else if (MIR_ConstString(#_v7) ==  name) { \
					return MIR::makeReflection(obj->_v7); \
				} \
				else if (MIR_ConstString(#_v8) ==  name) { \
					return MIR::makeReflection(obj->_v8); \
				} \
				else if (MIR_ConstString(#_v9) ==  name) { \
					return MIR::makeReflection(obj->_v9); \
				} \
				else if (MIR_ConstString(#_v10) ==  name) { \
					return MIR::makeReflection(obj->_v10); \
				} \
				else if (MIR_ConstString(#_v11) ==  name) { \
					return MIR::makeReflection(obj->_v11); \
				} \
				return MIR::Reflection(); \
			} \
			virtual uint32_t getFieldsCount(void* object) { return 12; } \
			virtual MIR::Reflection getFieldByNumber(void* object, uint32_t i) { \
				_classname* obj = (_classname*)object;  \
				switch(i) { \
					case 0: return MIR::makeReflection(obj->_v0); \
					case 1: return MIR::makeReflection(obj->_v1); \
					case 2: return MIR::makeReflection(obj->_v2); \
					case 3: return MIR::makeReflection(obj->_v3); \
					case 4: return MIR::makeReflection(obj->_v4); \
					case 5: return MIR::makeReflection(obj->_v5); \
					case 6: return MIR::makeReflection(obj->_v6); \
					case 7: return MIR::makeReflection(obj->_v7); \
					case 8: return MIR::makeReflection(obj->_v8); \
					case 9: return MIR::makeReflection(obj->_v9); \
					case 10: return MIR::makeReflection(obj->_v10); \
					case 11: return MIR::makeReflection(obj->_v11); \
				} \
				return MIR::Reflection(); \
			} \
			virtual MIR::ConstString getFieldName(void* object, uint32_t i) { \
				switch (i) { \
					case 0: return MIR_ConstString(#_v0); \
					case 1: return MIR_ConstString(#_v1); \
					case 2: return MIR_ConstString(#_v2); \
					case 3: return MIR_ConstString(#_v3); \
					case 4: return MIR_ConstString(#_v4); \
					case 5: return MIR_ConstString(#_v5); \
					case 6: return MIR_ConstString(#_v6); \
					case 7: return MIR_ConstString(#_v7); \
					case 8: return MIR_ConstString(#_v8); \
					case 9: return MIR_ConstString(#_v9); \
					case 10: return MIR_ConstString(#_v10); \
					case 11: return MIR_ConstString(#_v11); \
				} \
				return MIR::ConstString(); \
			} \
			virtual MIR::ConstString getClassName(void* object) { return MIR_ConstString(#_classname); } \
			virtual MIR::TypeId getTypeId(void* object) { return MIR::getTypeIdOf< _classname >(); } \
		}; \
		_classname() {


#define MICRO_DeclareData11(_classname, _base_class_code, _T0, _v0, _T1, _v1, _T2, _v2, _T3, _v3, _T4, _v4, _T5, _v5, _T6, _v6, _T7, _v7, _T8, _v8, _T9, _v9, _T10, _v10) \
class _classname _base_class_code { public: \
	_T0 _v0; \
	_T1 _v1; \
	_T2 _v2; \
	_T3 _v3; \
	_T4 _v4; \
	_T5 _v5; \
	_T6 _v6; \
	_T7 _v7; \
	_T8 _v8; \
	_T9 _v9; \
	_T10 _v10; \
	class Reflector : public MIR::IReflector { \
		public: \
			virtual MIR::Reflection getFieldByName(void* object, MIR::ConstString name) { \
				_classname* obj = (_classname*)object; \
				if (MIR_ConstString(#_v0) ==  name) { \
					return MIR::makeReflection(obj->_v0); \
				} \
				else if (MIR_ConstString(#_v1) ==  name) { \
					return MIR::makeReflection(obj->_v1); \
				} \
				else if (MIR_ConstString(#_v2) ==  name) { \
					return MIR::makeReflection(obj->_v2); \
				} \
				else if (MIR_ConstString(#_v3) ==  name) { \
					return MIR::makeReflection(obj->_v3); \
				} \
				else if (MIR_ConstString(#_v4) ==  name) { \
					return MIR::makeReflection(obj->_v4); \
				} \
				else if (MIR_ConstString(#_v5) ==  name) { \
					return MIR::makeReflection(obj->_v5); \
				} \
				else if (MIR_ConstString(#_v6) ==  name) { \
					return MIR::makeReflection(obj->_v6); \
				} \
				else if (MIR_ConstString(#_v7) ==  name) { \
					return MIR::makeReflection(obj->_v7); \
				} \
				else if (MIR_ConstString(#_v8) ==  name) { \
					return MIR::makeReflection(obj->_v8); \
				} \
				else if (MIR_ConstString(#_v9) ==  name) { \
					return MIR::makeReflection(obj->_v9); \
				} \
				else if (MIR_ConstString(#_v10) ==  name) { \
					return MIR::makeReflection(obj->_v10); \
				} \
				return MIR::Reflection(); \
			} \
			virtual uint32_t getFieldsCount(void* object) { return 11; } \
			virtual MIR::Reflection getFieldByNumber(void* object, uint32_t i) { \
				_classname* obj = (_classname*)object;  \
				switch(i) { \
					case 0: return MIR::makeReflection(obj->_v0); \
					case 1: return MIR::makeReflection(obj->_v1); \
					case 2: return MIR::makeReflection(obj->_v2); \
					case 3: return MIR::makeReflection(obj->_v3); \
					case 4: return MIR::makeReflection(obj->_v4); \
					case 5: return MIR::makeReflection(obj->_v5); \
					case 6: return MIR::makeReflection(obj->_v6); \
					case 7: return MIR::makeReflection(obj->_v7); \
					case 8: return MIR::makeReflection(obj->_v8); \
					case 9: return MIR::makeReflection(obj->_v9); \
					case 10: return MIR::makeReflection(obj->_v10); \
				} \
				return MIR::Reflection(); \
			} \
			virtual MIR::ConstString getFieldName(void* object, uint32_t i) { \
				switch (i) { \
					case 0: return MIR_ConstString(#_v0); \
					case 1: return MIR_ConstString(#_v1); \
					case 2: return MIR_ConstString(#_v2); \
					case 3: return MIR_ConstString(#_v3); \
					case 4: return MIR_ConstString(#_v4); \
					case 5: return MIR_ConstString(#_v5); \
					case 6: return MIR_ConstString(#_v6); \
					case 7: return MIR_ConstString(#_v7); \
					case 8: return MIR_ConstString(#_v8); \
					case 9: return MIR_ConstString(#_v9); \
					case 10: return MIR_ConstString(#_v10); \
				} \
				return MIR::ConstString(); \
			} \
			virtual MIR::ConstString getClassName(void* object) { return MIR_ConstString(#_classname); } \
			virtual MIR::TypeId getTypeId(void* object) { return MIR::getTypeIdOf< _classname >(); } \
		}; \
		_classname() {


#define MICRO_DeclareData10(_classname, _base_class_code, _T0, _v0, _T1, _v1, _T2, _v2, _T3, _v3, _T4, _v4, _T5, _v5, _T6, _v6, _T7, _v7, _T8, _v8, _T9, _v9) \
class _classname _base_class_code { public: \
	_T0 _v0; \
	_T1 _v1; \
	_T2 _v2; \
	_T3 _v3; \
	_T4 _v4; \
	_T5 _v5; \
	_T6 _v6; \
	_T7 _v7; \
	_T8 _v8; \
	_T9 _v9; \
	class Reflector : public MIR::IReflector { \
		public: \
			virtual MIR::Reflection getFieldByName(void* object, MIR::ConstString name) { \
				_classname* obj = (_classname*)object; \
				if (MIR_ConstString(#_v0) ==  name) { \
					return MIR::makeReflection(obj->_v0); \
				} \
				else if (MIR_ConstString(#_v1) ==  name) { \
					return MIR::makeReflection(obj->_v1); \
				} \
				else if (MIR_ConstString(#_v2) ==  name) { \
					return MIR::makeReflection(obj->_v2); \
				} \
				else if (MIR_ConstString(#_v3) ==  name) { \
					return MIR::makeReflection(obj->_v3); \
				} \
				else if (MIR_ConstString(#_v4) ==  name) { \
					return MIR::makeReflection(obj->_v4); \
				} \
				else if (MIR_ConstString(#_v5) ==  name) { \
					return MIR::makeReflection(obj->_v5); \
				} \
				else if (MIR_ConstString(#_v6) ==  name) { \
					return MIR::makeReflection(obj->_v6); \
				} \
				else if (MIR_ConstString(#_v7) ==  name) { \
					return MIR::makeReflection(obj->_v7); \
				} \
				else if (MIR_ConstString(#_v8) ==  name) { \
					return MIR::makeReflection(obj->_v8); \
				} \
				else if (MIR_ConstString(#_v9) ==  name) { \
					return MIR::makeReflection(obj->_v9); \
				} \
				return MIR::Reflection(); \
			} \
			virtual uint32_t getFieldsCount(void* object) { return 10; } \
			virtual MIR::Reflection getFieldByNumber(void* object, uint32_t i) { \
				_classname* obj = (_classname*)object;  \
				switch(i) { \
					case 0: return MIR::makeReflection(obj->_v0); \
					case 1: return MIR::makeReflection(obj->_v1); \
					case 2: return MIR::makeReflection(obj->_v2); \
					case 3: return MIR::makeReflection(obj->_v3); \
					case 4: return MIR::makeReflection(obj->_v4); \
					case 5: return MIR::makeReflection(obj->_v5); \
					case 6: return MIR::makeReflection(obj->_v6); \
					case 7: return MIR::makeReflection(obj->_v7); \
					case 8: return MIR::makeReflection(obj->_v8); \
					case 9: return MIR::makeReflection(obj->_v9); \
				} \
				return MIR::Reflection(); \
			} \
			virtual MIR::ConstString getFieldName(void* object, uint32_t i) { \
				switch (i) { \
					case 0: return MIR_ConstString(#_v0); \
					case 1: return MIR_ConstString(#_v1); \
					case 2: return MIR_ConstString(#_v2); \
					case 3: return MIR_ConstString(#_v3); \
					case 4: return MIR_ConstString(#_v4); \
					case 5: return MIR_ConstString(#_v5); \
					case 6: return MIR_ConstString(#_v6); \
					case 7: return MIR_ConstString(#_v7); \
					case 8: return MIR_ConstString(#_v8); \
					case 9: return MIR_ConstString(#_v9); \
				} \
				return MIR::ConstString(); \
			} \
			virtual MIR::ConstString getClassName(void* object) { return MIR_ConstString(#_classname); } \
			virtual MIR::TypeId getTypeId(void* object) { return MIR::getTypeIdOf< _classname >(); } \
		}; \
		_classname() {


#define MICRO_DeclareData9(_classname,  _T0, _v0, _T1, _v1, _T2, _v2, _T3, _v3, _T4, _v4, _T5, _v5, _T6, _v6, _T7, _v7, _T8, _v8) \
class _classname _base_class_code { public: \
	_T0 _v0; \
	_T1 _v1; \
	_T2 _v2; \
	_T3 _v3; \
	_T4 _v4; \
	_T5 _v5; \
	_T6 _v6; \
	_T7 _v7; \
	_T8 _v8; \
	class Reflector : public MIR::IReflector { \
		public: \
			virtual MIR::Reflection getFieldByName(void* object, MIR::ConstString name) { \
				_classname* obj = (_classname*)object; \
				if (MIR_ConstString(#_v0) ==  name) { \
					return MIR::makeReflection(obj->_v0); \
				} \
				else if (MIR_ConstString(#_v1) ==  name) { \
					return MIR::makeReflection(obj->_v1); \
				} \
				else if (MIR_ConstString(#_v2) ==  name) { \
					return MIR::makeReflection(obj->_v2); \
				} \
				else if (MIR_ConstString(#_v3) ==  name) { \
					return MIR::makeReflection(obj->_v3); \
				} \
				else if (MIR_ConstString(#_v4) ==  name) { \
					return MIR::makeReflection(obj->_v4); \
				} \
				else if (MIR_ConstString(#_v5) ==  name) { \
					return MIR::makeReflection(obj->_v5); \
				} \
				else if (MIR_ConstString(#_v6) ==  name) { \
					return MIR::makeReflection(obj->_v6); \
				} \
				else if (MIR_ConstString(#_v7) ==  name) { \
					return MIR::makeReflection(obj->_v7); \
				} \
				else if (MIR_ConstString(#_v8) ==  name) { \
					return MIR::makeReflection(obj->_v8); \
				} \
				return MIR::Reflection(); \
			} \
			virtual uint32_t getFieldsCount(void* object) { return 9; } \
			virtual MIR::Reflection getFieldByNumber(void* object, uint32_t i) { \
				_classname* obj = (_classname*)object;  \
				switch(i) { \
					case 0: return MIR::makeReflection(obj->_v0); \
					case 1: return MIR::makeReflection(obj->_v1); \
					case 2: return MIR::makeReflection(obj->_v2); \
					case 3: return MIR::makeReflection(obj->_v3); \
					case 4: return MIR::makeReflection(obj->_v4); \
					case 5: return MIR::makeReflection(obj->_v5); \
					case 6: return MIR::makeReflection(obj->_v6); \
					case 7: return MIR::makeReflection(obj->_v7); \
					case 8: return MIR::makeReflection(obj->_v8); \
				} \
				return MIR::Reflection(); \
			} \
			virtual MIR::ConstString getFieldName(void* object, uint32_t i) { \
				switch (i) { \
					case 0: return MIR_ConstString(#_v0); \
					case 1: return MIR_ConstString(#_v1); \
					case 2: return MIR_ConstString(#_v2); \
					case 3: return MIR_ConstString(#_v3); \
					case 4: return MIR_ConstString(#_v4); \
					case 5: return MIR_ConstString(#_v5); \
					case 6: return MIR_ConstString(#_v6); \
					case 7: return MIR_ConstString(#_v7); \
					case 8: return MIR_ConstString(#_v8); \
				} \
				return MIR::ConstString(); \
			} \
			virtual MIR::ConstString getClassName(void* object) { return MIR_ConstString(#_classname); } \
			virtual MIR::TypeId getTypeId(void* object) { return MIR::getTypeIdOf< _classname >(); } \
		}; \
		_classname() {

#define MICRO_DeclareData8(_classname,  _base_class_code,  _T0,  _v0, _T1, _v1, _T2, _v2, _T3, _v3, _T4, _v4, _T5, _v5, _T6, _v6, _T7, _v7) \
class _classname _base_class_code { public: \
	_T0 _v0; \
	_T1 _v1; \
	_T2 _v2; \
	_T3 _v3; \
	_T4 _v4; \
	_T5 _v5; \
	_T6 _v6; \
	_T7 _v7; \
	class Reflector : public MIR::IReflector { \
		public: \
			virtual MIR::Reflection getFieldByName(void* object, MIR::ConstString name) { \
				_classname* obj = (_classname*)object; \
				if (MIR_ConstString(#_v0) ==  name) { \
					return MIR::makeReflection(obj->_v0); \
				} \
				else if (MIR_ConstString(#_v1) ==  name) { \
					return MIR::makeReflection(obj->_v1); \
				} \
				else if (MIR_ConstString(#_v2) ==  name) { \
					return MIR::makeReflection(obj->_v2); \
				} \
				else if (MIR_ConstString(#_v3) ==  name) { \
					return MIR::makeReflection(obj->_v3); \
				} \
				else if (MIR_ConstString(#_v4) ==  name) { \
					return MIR::makeReflection(obj->_v4); \
				} \
				else if (MIR_ConstString(#_v5) ==  name) { \
					return MIR::makeReflection(obj->_v5); \
				} \
				else if (MIR_ConstString(#_v6) ==  name) { \
					return MIR::makeReflection(obj->_v6); \
				} \
				else if (MIR_ConstString(#_v7) ==  name) { \
					return MIR::makeReflection(obj->_v7); \
				} \
				return MIR::Reflection(); \
			} \
			virtual uint32_t getFieldsCount(void* object) { return 8; } \
			virtual MIR::Reflection getFieldByNumber(void* object, uint32_t i) { \
				_classname* obj = (_classname*)object;  \
				switch(i) { \
					case 0: return MIR::makeReflection(obj->_v0); \
					case 1: return MIR::makeReflection(obj->_v1); \
					case 2: return MIR::makeReflection(obj->_v2); \
					case 3: return MIR::makeReflection(obj->_v3); \
					case 4: return MIR::makeReflection(obj->_v4); \
					case 5: return MIR::makeReflection(obj->_v5); \
					case 6: return MIR::makeReflection(obj->_v6); \
					case 7: return MIR::makeReflection(obj->_v7); \
				} \
				return MIR::Reflection(); \
			} \
			virtual MIR::ConstString getFieldName(void* object, uint32_t i) { \
				switch (i) { \
					case 0: return MIR_ConstString(#_v0); \
					case 1: return MIR_ConstString(#_v1); \
					case 2: return MIR_ConstString(#_v2); \
					case 3: return MIR_ConstString(#_v3); \
					case 4: return MIR_ConstString(#_v4); \
					case 5: return MIR_ConstString(#_v5); \
					case 6: return MIR_ConstString(#_v6); \
					case 7: return MIR_ConstString(#_v7); \
				} \
				return MIR::ConstString(); \
			} \
			virtual MIR::ConstString getClassName(void* object) { return MIR_ConstString(#_classname); } \
			virtual MIR::TypeId getTypeId(void* object) { return MIR::getTypeIdOf<_classname>(); } \
		}; \
		_classname() {

#define MICRO_DeclareData7(_classname, _base_class_code, _T0, _v0, _T1, _v1, _T2, _v2, _T3, _v3, _T4, _v4, _T5, _v5, _T6, _v6) \
class _classname _base_class_code { \
public: \
	_T0 _v0; \
	_T1 _v1; \
	_T2 _v2; \
	_T3 _v3; \
	_T4 _v4; \
	_T5 _v5; \
	_T6 _v6; \
	class Reflector : public MIR::IReflector { \
		public: \
			virtual MIR::Reflection getFieldByName(void* object, MIR::ConstString name) { \
				_classname* obj = (_classname*)object; \
				if (MIR_ConstString(#_v0) ==  name) { \
					return MIR::makeReflection(obj->_v0); \
				} \
				else if (MIR_ConstString(#_v1) ==  name) { \
					return MIR::makeReflection(obj->_v1); \
				} \
				else if (MIR_ConstString(#_v2) ==  name) { \
					return MIR::makeReflection(obj->_v2); \
				} \
				else if (MIR_ConstString(#_v3) ==  name) { \
					return MIR::makeReflection(obj->_v3); \
				} \
				else if (MIR_ConstString(#_v4) ==  name) { \
					return MIR::makeReflection(obj->_v4); \
				} \
				else if (MIR_ConstString(#_v5) ==  name) { \
					return MIR::makeReflection(obj->_v5); \
				} \
				else if (MIR_ConstString(#_v6) ==  name) { \
					return MIR::makeReflection(obj->_v6); \
				} \
				return MIR::Reflection(); \
			} \
			virtual uint32_t getFieldsCount(void* object) { return 7; } \
			virtual MIR::Reflection getFieldByNumber(void* object, uint32_t i) { \
				_classname* obj = (_classname*)object;  \
				switch(i) { \
					case 0: return MIR::makeReflection(obj->_v0); \
					case 1: return MIR::makeReflection(obj->_v1); \
					case 2: return MIR::makeReflection(obj->_v2); \
					case 3: return MIR::makeReflection(obj->_v3); \
					case 4: return MIR::makeReflection(obj->_v4); \
					case 5: return MIR::makeReflection(obj->_v5); \
					case 6: return MIR::makeReflection(obj->_v6); \
				} \
				return MIR::Reflection(); \
			} \
			virtual MIR::ConstString getFieldName(void* object, uint32_t i) { \
				switch (i) { \
					case 0: return MIR_ConstString(#_v0); \
					case 1: return MIR_ConstString(#_v1); \
					case 2: return MIR_ConstString(#_v2); \
					case 3: return MIR_ConstString(#_v3); \
					case 4: return MIR_ConstString(#_v4); \
					case 5: return MIR_ConstString(#_v5); \
					case 6: return MIR_ConstString(#_v6); \
				} \
				return MIR::ConstString(); \
			} \
			virtual MIR::ConstString getClassName(void* object) { return MIR_ConstString(#_classname); } \
			virtual MIR::TypeId getTypeId(void* object) { return MIR::getTypeIdOf<_classname>(); } \
		}; \
		_classname() {


#define MICRO_DeclareData6(_classname,  _base_class_code,  _T0,  _v0, _T1, _v1, _T2, _v2, _T3, _v3, _T4, _v4, _T5, _v5) \
class _classname _base_class_code { public: \
	_T0 _v0; \
	_T1 _v1; \
	_T2 _v2; \
	_T3 _v3; \
	_T4 _v4; \
	_T5 _v5; \
	class Reflector : public MIR::IReflector { \
		public: \
			virtual MIR::Reflection getFieldByName(void* object, MIR::ConstString name) { \
				_classname* obj = (_classname*)object; \
				if (MIR_ConstString(#_v0) ==  name) { \
					return MIR::makeReflection(obj->_v0); \
				} \
				else if (MIR_ConstString(#_v1) ==  name) { \
					return MIR::makeReflection(obj->_v1); \
				} \
				else if (MIR_ConstString(#_v2) ==  name) { \
					return MIR::makeReflection(obj->_v2); \
				} \
				else if (MIR_ConstString(#_v3) ==  name) { \
					return MIR::makeReflection(obj->_v3); \
				} \
				else if (MIR_ConstString(#_v4) ==  name) { \
					return MIR::makeReflection(obj->_v4); \
				} \
				else if (MIR_ConstString(#_v5) ==  name) { \
					return MIR::makeReflection(obj->_v5); \
				} \
				return MIR::Reflection(); \
			} \
			virtual uint32_t getFieldsCount(void* object) { return 6; } \
			virtual MIR::Reflection getFieldByNumber(void* object, uint32_t i) { \
				_classname* obj = (_classname*)object;  \
				switch(i) { \
					case 0: return MIR::makeReflection(obj->_v0); \
					case 1: return MIR::makeReflection(obj->_v1); \
					case 2: return MIR::makeReflection(obj->_v2); \
					case 3: return MIR::makeReflection(obj->_v3); \
					case 4: return MIR::makeReflection(obj->_v4); \
					case 5: return MIR::makeReflection(obj->_v5); \
				} \
				return MIR::Reflection(); \
			} \
			virtual MIR::ConstString getFieldName(void* object, uint32_t i) { \
				switch (i) { \
					case 0: return MIR_ConstString(#_v0); \
					case 1: return MIR_ConstString(#_v1); \
					case 2: return MIR_ConstString(#_v2); \
					case 3: return MIR_ConstString(#_v3); \
					case 4: return MIR_ConstString(#_v4); \
					case 5: return MIR_ConstString(#_v5); \
				} \
				return MIR::ConstString(); \
			} \
			virtual MIR::ConstString getClassName(void* object) { return MIR_ConstString(#_classname); } \
			virtual MIR::TypeId getTypeId(void* object) { return MIR::getTypeIdOf<_classname>(); } \
		}; \
		_classname() {


#define MICRO_DeclareData5(_classname,  _base_class_code,  _T0,  _v0, _T1, _v1, _T2, _v2, _T3, _v3, _T4, _v4) \
class _classname _base_class_code { public: \
	_T0 _v0; \
	_T1 _v1; \
	_T2 _v2; \
	_T3 _v3; \
	_T4 _v4; \
	class Reflector : public MIR::IReflector { \
		public: \
			virtual MIR::Reflection getFieldByName(void* object, MIR::ConstString name) { \
				_classname* obj = (_classname*)object; \
				if (MIR_ConstString(#_v0) ==  name) { \
					return MIR::makeReflection(obj->_v0); \
				} \
				else if (MIR_ConstString(#_v1) ==  name) { \
					return MIR::makeReflection(obj->_v1); \
				} \
				else if (MIR_ConstString(#_v2) ==  name) { \
					return MIR::makeReflection(obj->_v2); \
				} \
				else if (MIR_ConstString(#_v3) ==  name) { \
					return MIR::makeReflection(obj->_v3); \
				} \
				else if (MIR_ConstString(#_v4) ==  name) { \
					return MIR::makeReflection(obj->_v4); \
				} \
				return MIR::Reflection(); \
			} \
			virtual uint32_t getFieldsCount(void* object) { return 5; } \
			virtual MIR::Reflection getFieldByNumber(void* object, uint32_t i) { \
				_classname* obj = (_classname*)object;  \
				switch(i) { \
					case 0: return MIR::makeReflection(obj->_v0); \
					case 1: return MIR::makeReflection(obj->_v1); \
					case 2: return MIR::makeReflection(obj->_v2); \
					case 3: return MIR::makeReflection(obj->_v3); \
					case 4: return MIR::makeReflection(obj->_v4); \
				} \
				return MIR::Reflection(); \
			} \
			virtual MIR::ConstString getFieldName(void* object, uint32_t i) { \
				switch (i) { \
					case 0: return MIR_ConstString(#_v0); \
					case 1: return MIR_ConstString(#_v1); \
					case 2: return MIR_ConstString(#_v2); \
					case 3: return MIR_ConstString(#_v3); \
					case 4: return MIR_ConstString(#_v4); \
				} \
				return MIR::ConstString(); \
			} \
			virtual MIR::ConstString getClassName(void* object) { return MIR_ConstString(#_classname); } \
			virtual MIR::TypeId getTypeId(void* object) { return MIR::getTypeIdOf<_classname>(); } \
		}; \
		_classname() {

#define MICRO_DeclareData4(_classname,  _base_class_code,  _T0,  _v0, _T1, _v1, _T2, _v2, _T3, _v3) \
class _classname _base_class_code { public: \
	_T0 _v0; \
	_T1 _v1; \
	_T2 _v2; \
	_T3 _v3; \
	class Reflector : public MIR::IReflector { \
		public: \
			virtual MIR::Reflection getFieldByName(void* object, MIR::ConstString name) { \
				_classname* obj = (_classname*)object; \
				if (MIR_ConstString(#_v0) ==  name) { \
					return MIR::makeReflection(obj->_v0); \
				} \
				else if (MIR_ConstString(#_v1) ==  name) { \
					return MIR::makeReflection(obj->_v1); \
				} \
				else if (MIR_ConstString(#_v2) ==  name) { \
					return MIR::makeReflection(obj->_v2); \
				} \
				else if (MIR_ConstString(#_v3) ==  name) { \
					return MIR::makeReflection(obj->_v3); \
				} \
				return MIR::Reflection(); \
			} \
			virtual uint32_t getFieldsCount(void* object) { return 4; } \
			virtual MIR::Reflection getFieldByNumber(void* object, uint32_t i) { \
				_classname* obj = (_classname*)object;  \
				switch(i) { \
					case 0: return MIR::makeReflection(obj->_v0); \
					case 1: return MIR::makeReflection(obj->_v1); \
					case 2: return MIR::makeReflection(obj->_v2); \
					case 3: return MIR::makeReflection(obj->_v3); \
				} \
				return MIR::Reflection(); \
			} \
			virtual MIR::ConstString getFieldName(void* object, uint32_t i) { \
				switch (i) { \
					case 0: return MIR_ConstString(#_v0); \
					case 1: return MIR_ConstString(#_v1); \
					case 2: return MIR_ConstString(#_v2); \
					case 3: return MIR_ConstString(#_v3); \
				} \
				return MIR::ConstString(); \
			} \
			virtual MIR::ConstString getClassName(void* object) { return MIR_ConstString(#_classname); } \
			virtual MIR::TypeId getTypeId(void* object) { return MIR::getTypeIdOf<_classname>(); } \
		}; \
		_classname() {

#define MICRO_DeclareData3(_classname,  _base_class_code,  _T0,  _v0, _T1, _v1, _T2, _v2) \
class _classname _base_class_code { public: \
	_T0 _v0; \
	_T1 _v1; \
	_T2 _v2; \
	class Reflector : public MIR::IReflector { \
		public: \
			virtual MIR::Reflection getFieldByName(void* object, MIR::ConstString name) { \
				_classname* obj = (_classname*)object; \
				if (MIR_ConstString(#_v0) ==  name) { \
					return MIR::makeReflection(obj->_v0); \
				} \
				else if (MIR_ConstString(#_v1) ==  name) { \
					return MIR::makeReflection(obj->_v1); \
				} \
				else if (MIR_ConstString(#_v2) ==  name) { \
					return MIR::makeReflection(obj->_v2); \
				} \
				return MIR::Reflection(); \
			} \
			virtual uint32_t getFieldsCount(void* object) { return 3; } \
			virtual MIR::Reflection getFieldByNumber(void* object, uint32_t i) { \
				_classname* obj = (_classname*)object;  \
				switch(i) { \
					case 0: return MIR::makeReflection(obj->_v0); \
					case 1: return MIR::makeReflection(obj->_v1); \
					case 2: return MIR::makeReflection(obj->_v2); \
				} \
				return MIR::Reflection(); \
			} \
			virtual MIR::ConstString getFieldName(void* object, uint32_t i) { \
				switch (i) { \
					case 0: return MIR_ConstString(#_v0); \
					case 1: return MIR_ConstString(#_v1); \
					case 2: return MIR_ConstString(#_v2); \
				} \
				return MIR::ConstString(); \
			} \
			virtual MIR::ConstString getClassName(void* object) { return MIR_ConstString(#_classname); } \
			virtual MIR::TypeId getTypeId(void* object) { return MIR::getTypeIdOf<_classname>(); } \
		}; \
		_classname() {

#define MICRO_DeclareData2(_classname,  _base_class_code,  _T0,  _v0, _T1, _v1) \
class _classname _base_class_code { public: \
	_T0 _v0; \
	_T1 _v1; \
	class Reflector : public MIR::IReflector { \
		public: \
			virtual MIR::Reflection getFieldByName(void* object, MIR::ConstString name) { \
				_classname* obj = (_classname*)object; \
				if (MIR_ConstString(#_v0) ==  name) { \
					return MIR::makeReflection(obj->_v0); \
				} \
				else if (MIR_ConstString(#_v1) ==  name) { \
					return MIR::makeReflection(obj->_v1); \
				} \
				return MIR::Reflection(); \
			} \
			virtual uint32_t getFieldsCount(void* object) { return 2; } \
			virtual MIR::Reflection getFieldByNumber(void* object, uint32_t i) { \
				_classname* obj = (_classname*)object;  \
				switch(i) { \
					case 0: return MIR::makeReflection(obj->_v0); \
					case 1: return MIR::makeReflection(obj->_v1); \
				} \
				return MIR::Reflection(); \
			} \
			virtual MIR::ConstString getFieldName(void* object, uint32_t i) { \
				switch (i) { \
					case 0: return MIR_ConstString( #_v0 ); \
					case 1: return MIR_ConstString(#_v1); \
				} \
				return MIR::ConstString(); \
			} \
			virtual MIR::ConstString getClassName(void* object) { return MIR_ConstString(#_classname); } \
			virtual MIR::TypeId getTypeId(void* object) { return MIR::getTypeIdOf< _classname >(); } \
		}; \
		_classname() {

#define MICRO_DeclareData1(_classname,  _base_class_code,  _T0,  _v0) \
class _classname _base_class_code { public: \
	_T0 _v0; \
	class Reflector : public MIR::IReflector { \
		public: \
			virtual MIR::Reflection getFieldByName(void* object, MIR::ConstString name) { \
				_classname* obj = (_classname*)object; \
				if (MIR_ConstString(#_v0) ==  name) { \
					return MIR::makeReflection(obj->_v0); \
				} \
				return MIR::Reflection(); \
			} \
			virtual uint32_t getFieldsCount(void* object) { return 1; } \
			virtual MIR::Reflection getFieldByNumber(void* object, uint32_t i) { \
				_classname* obj = (_classname*)object;  \
				switch(i) { \
					case 0: return MIR::makeReflection(obj->_v0); \
				} \
				return MIR::Reflection(); \
			} \
			virtual MIR::ConstString getFieldName(void* object, uint32_t i) { \
				switch (i) { \
					case 0: return MIR_ConstString(#_v0); \
				} \
				return MIR::ConstString(); \
			} \
			virtual MIR::ConstString getClassName(void* object) { return MIR_ConstString(#_classname); } \
			virtual MIR::TypeId getTypeId(void* object) { return MIR::getTypeIdOf<_classname>(); } \
		}; \
		_classname() {



#define MICRO_DeclareData0(_classname,  _base_class_code) \
class _classname _base_class_code { public: \
	class Reflector : public MIR::IReflector { \
		public: \
			virtual MIR::Reflection getFieldByName(void* object, MIR::ConstString name) { \
				return MIR::Reflection(); \
			} \
			virtual uint32_t getFieldsCount(void* object) { return 0; } \
			virtual MIR::Reflection getFieldByNumber(void* object, uint32_t i) { \
				return MIR::Reflection(); \
			} \
			virtual MIR::ConstString getFieldName(void* object, uint32_t i) { \
				return MIR::ConstString(); \
			} \
			virtual MIR::ConstString getClassName(void* object) { return MIR_ConstString(#_classname); } \
			virtual MIR::TypeId getTypeId(void* object) { return MIR::getTypeIdOf<_classname>(); } \
		}; \
		_classname() {


#define MICRO_DeclareReflactiveBaseClass1(_classname, _baseclassname) \
	virtual uint32_t getBaseClassesCount(void* object) { return 1; } \
	virtual MIR::Reflection getBaseClass(void* object, uint32_t i) { \
		_classname* obj = (_classname*)object;  \
		switch(i) { \
			case 0: return MIR::makeReflection(*static_cast<_baseclassname*>(obj)); \
		} \
		return MIR::Reflection(); \
	}

