#pragma once

#include "mir_reflection.h"

namespace MIR {

class ValueTypeReflectorBase : IReflector {
public:
	virtual EMetaType getMetaType(void* object) { return EMetaType_Value; }
	virtual uint32_t getFieldsCount(void* object){ return 0; }
	virtual ConstString getFieldName(void* object, uint32_t i){ return ConstString(); }
	virtual Reflection getFieldByName(void* object, ConstString name){ return Reflection(); }
	virtual Reflection getFieldByOffset(void* object, uint32_t offset){ return Reflection(); }
	virtual Reflection getFieldByNumber(void* object, uint32_t i){ return Reflection(); }
	virtual ConstString getClassName(void* object){ return ConstString(); }
};

class FloatValueTypeReflector : ValueTypeReflectorBase {
public:
	virtual TypeId getTypeId(void* object) { return getTypeIdOf<float>(); }
	virtual bool print(void* object, IWriter& w, FormattingParams& fp) {
		ValueFormatStringBuff buff;
		buff.print(*(const float*)object);
		return w.write(buff.begin(), buff.size());
	}
	virtual bool scan(void* object, IReader& rb , FormattingParams& fp) {
		ValueFormatStringBuff buff;
		if (!scan_value(buff, rb))return false;
		return buff.parse(*(float*)object);
	}
};

static inline Reflection makeReflection(float& obj) {
	static_assert(sizeof(FloatValueTypeReflector) <= sizeof(void*));
	Reflection r;
	r.object_ptr = &obj;
	new (&r.reflector_mem) FloatValueTypeReflector();
	return r;
}


class Int32ValueTypeReflector : ValueTypeReflectorBase {
public:
	virtual TypeId getTypeId(void* object) { return getTypeIdOf<int32_t>(); }
	virtual bool print(void* object, IWriter& w, FormattingParams& fp) {
		ValueFormatStringBuff buff;
		buff.print(*(const int32_t*)object);
		return w.write(buff.begin(), buff.size());
	}
	virtual bool scan(void* object, IReader& rb , FormattingParams& fp) {
		ValueFormatStringBuff buff;
		if (!scan_value(buff, rb))return false;
		return buff.parse(*(int32_t*)object);
	}
};

static inline Reflection makeReflection(int32_t& obj) {
	static_assert(sizeof(Int32ValueTypeReflector) <= sizeof(void*));
	Reflection r;
	r.object_ptr = &obj;
	new (&r.reflector_mem) Int32ValueTypeReflector();
	return r;
}

static inline Reflection makeReflection(int& obj) {
	if (sizeof(int) == 4) {
		return makeReflection(*reinterpret_cast<int32_t*>(&obj));
	}
	return makeInvalidReflaction();
}

class Uint32ValueTypeReflector : ValueTypeReflectorBase {
public:
	virtual TypeId getTypeId(void* object) { return getTypeIdOf<uint32_t>(); }
	virtual bool print(void* object, IWriter& w, FormattingParams& fp) {
		ValueFormatStringBuff buff;
		buff.print(*(const uint32_t*)object);
		return w.write(buff.begin(), buff.size());
	}
	virtual bool scan(void* object, IReader& rb , FormattingParams& fp) {
		ValueFormatStringBuff buff;
		if (!scan_value(buff, rb))return false;
		return buff.parse(*(uint32_t*)object);
	}
};

static inline Reflection makeReflection(uint32_t& obj) {
	static_assert(sizeof(Uint32ValueTypeReflector) <= sizeof(void*));
	Reflection r;
	r.object_ptr = &obj;
	new (&r.reflector_mem) Uint32ValueTypeReflector();
	return r;
}

static inline Reflection makeReflection(uint& obj) {
	if (sizeof(uint) == 4) {
		return makeReflection(*reinterpret_cast<uint32_t*>(&obj));
	}
	return makeInvalidReflaction();
}


class Int16ValueTypeReflector : ValueTypeReflectorBase {
public:
	virtual TypeId getTypeId(void* object) { return getTypeIdOf<int16_t>(); }
	virtual bool print(void* object, IWriter& w, FormattingParams& fp) {
		ValueFormatStringBuff buff;
		buff.print(*(const int16_t*)object);
		return w.write(buff.begin(), buff.size());
	}
	virtual bool scan(void* object, IReader& rb , FormattingParams& fp) {
		ValueFormatStringBuff buff;
		if (!scan_value(buff, rb))return false;
		return buff.parse(*(int16_t*)object);
	}
};

static inline Reflection makeReflection(int16_t& obj) {
	static_assert(sizeof(Int16ValueTypeReflector) <= sizeof(void*));
	Reflection r;
	r.object_ptr = &obj;
	new (&r.reflector_mem) Int16ValueTypeReflector();
	return r;
}

class Uint16ValueTypeReflector : ValueTypeReflectorBase {
public:
	virtual TypeId getTypeId(void* object) { return getTypeIdOf<uint16_t>(); }
	virtual bool print(void* object, IWriter& w, FormattingParams& fp) {
		ValueFormatStringBuff buff;
		buff.print(*(const uint16_t*)object);
		return w.write(buff.begin(), buff.size());
	}
	virtual bool scan(void* object, IReader& rb , FormattingParams& fp) {
		ValueFormatStringBuff buff;
		if (!scan_value(buff, rb))return false;
		return buff.parse(*(uint16_t*)object);
	}
};

static inline Reflection makeReflection(uint16_t& obj) {
	static_assert(sizeof(Uint16ValueTypeReflector) <= sizeof(void*));
	Reflection r;
	r.object_ptr = &obj;
	new (&r.reflector_mem) Uint16ValueTypeReflector();
	return r;
}


class Int8ValueTypeReflector : ValueTypeReflectorBase {
public:
	virtual TypeId getTypeId(void* object) { return getTypeIdOf<int8_t>(); }
	virtual bool print(void* object, IWriter& w, FormattingParams& fp) {
		ValueFormatStringBuff buff;
		buff.print(*(const int8_t*)object);
		return w.write(buff.begin(), buff.size());
	}
	virtual bool scan(void* object, IReader& rb , FormattingParams& fp) {
		ValueFormatStringBuff buff;
		if (!scan_value(buff, rb))return false;
		return buff.parse(*(int8_t*)object);
	}
};

static inline Reflection makeReflection(int8_t& obj) {
	static_assert(sizeof(Int8ValueTypeReflector) <= sizeof(void*));
	Reflection r;
	r.object_ptr = &obj;
	new (&r.reflector_mem) Int8ValueTypeReflector();
	return r;
}

class Uint8ValueTypeReflector : ValueTypeReflectorBase {
public:
	virtual TypeId getTypeId(void* object) { return getTypeIdOf<uint8_t>(); }
	virtual bool print(void* object, IWriter& w, FormattingParams& fp) {
		ValueFormatStringBuff buff;
		buff.print(*(const uint8_t*)object);
		return w.write(buff.begin(), buff.size());
	}
	virtual bool scan(void* object, IReader& rb , FormattingParams& fp) {
		ValueFormatStringBuff buff;
		if (!scan_value(buff, rb))return false;
		return buff.parse(*(uint8_t*)object);
	}
};

static inline Reflection makeReflection(uint8_t& obj) {
	static_assert(sizeof(Uint8ValueTypeReflector) <= sizeof(void*));
	Reflection r;
	r.object_ptr = &obj;
	new (&r.reflector_mem) Uint8ValueTypeReflector();
	return r;
}


class BoolValueTypeReflector : ValueTypeReflectorBase {
public:
	virtual TypeId getTypeId(void* object) { return getTypeIdOf<bool>(); }
	virtual bool print(void* object, IWriter& w, FormattingParams& fp) {
		ValueFormatStringBuff buff;
		buff.print(*(const bool*)object);
		return w.write(buff.begin(), buff.size());
	}
	virtual bool scan(void* object, IReader& rb, FormattingParams& fp) {
		ValueFormatStringBuff buff;
		if (!scan_value(buff, rb))return false;
		return buff.parse(*(bool*)object);
	}
};

static inline Reflection makeReflection(bool& obj) {
	static_assert(sizeof(BoolValueTypeReflector) <= sizeof(void*));
	Reflection r;
	r.object_ptr = &obj;
	new (&r.reflector_mem) BoolValueTypeReflector();
	return r;
}

template<uint32_t t_capacity>
class StringBuffReflector : ValueTypeReflectorBase {
public:
	typedef StringBuff<t_capacity> StringBuff_t;

	virtual TypeId getTypeId(void* object) { return getTypeIdOf<StringBuff_t>(); }
	virtual uint32_t getCapacity(void* object) { return StringBuff_t::capacity; }
	virtual bool print(void* object, IWriter& w, FormattingParams& fp) {
		StringBuff_t& obj = *(StringBuff_t*)object;
		return w.write("\"") && w.write(obj.begin(), obj.size()) && w.write("\"");
	}
	virtual bool scan(void* object, IReader& reader, FormattingParams& fp) {
		char c;
		do {
			if (!reader.read(&c))return false;
		} while(c != '"');

		StringBuff_t& obj = *(StringBuff_t*)object;
		while(true) {
			if (obj.isFull())return false;
			if (!reader.read(&c))return false;
			if (c == '"') return true;
			obj.append(c);
		};
	}
};

template<uint32_t t_capacity>
static inline Reflection makeReflection(StringBuff<t_capacity>& obj) {
	static_assert(sizeof(StringBuffReflector<t_capacity>) <= sizeof(void*));
	Reflection r;
	r.object_ptr = &obj;
	new (&r.reflector_mem) StringBuffReflector<t_capacity>();
	return r;
}


template<class t_T>
static inline Reflection makeReflection(t_T& obj) {
	static_assert(sizeof(typename t_T::Reflector) <= sizeof(void*));
	Reflection r;
	r.object_ptr = &obj;
	new (&r.reflector_mem) typename t_T::Reflector();
	return r;
}

template<class t_T>
static inline Reflection makeTypeReflection() {
	return makeReflection(*(t_T*)nullptr);
}

}
