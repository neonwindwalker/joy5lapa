#pragma once

#include "mir_reflection.h"
#include "mir_string.h"
#include "mir_array.h"
#include "mir_macro_declarators.h"
#include "mir_reflections.h"
#include "mir_poly.h"

namespace MIR {

extern bool console_cmd_ls_exec(IWriter& writer, Reflection rootReflection, StringBuff<256>& workDir, int argc, const char* const* argv);
extern bool console_cmd_cd_exec(Reflection rootReflection, StringBuff<256>& workDir, int argc, const char* const* argv);
extern bool console_cmd_print_exec(IWriter& writer, Reflection rootReflection, StringBuff<256>& workDir, FormattingParams formattingParams, int argc, const char* const* argv);
extern bool console_cmd_set_exec(Reflection rootReflection, StringBuff<256>& workDir, int argc, const char* const* argv);
extern bool console_cmd_scan_exec(IReader& reader, Reflection rootReflection, StringBuff<256>& workDir, FormattingParams formattingParams, int argc, const char* const* argv);

}

