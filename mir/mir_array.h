#pragma once

#include "mir_reflections.h"

namespace MIR {

template<typename t_DataType, uint32_t t_size>
class FixedArray {
private:
	t_DataType arr[t_size];
public:
	typedef FixedArray<t_DataType, t_size> FixedArray_t;
	typedef t_DataType DataType_t;
	static const uint32_t size = t_size;

	DataType_t& operator [] (int i) { return arr[i]; }
	const DataType_t& operator [] (int i)const { return arr[i]; }
	DataType_t& at(int i) { return arr[i]; }
	const DataType_t& at(int i)const { return arr[i]; }

	class Reflector : public IReflector {
	public:
		virtual uint32_t getFieldsCount(void* object) { return t_size; }
		virtual ConstString getFieldName(void* object, uint32_t i) { return ConstString(); }
		virtual Reflection getFieldByName(void* object, ConstString name) { return Reflection(); }
		virtual Reflection getFieldByOffset(void* object, uint32_t offset) { return makeReflection((*reinterpret_cast<FixedArray_t*>(object))[offset / sizeof(t_DataType)]); }
		virtual Reflection getFieldByNumber(void* object, uint32_t i) { return makeReflection((*reinterpret_cast<FixedArray_t*>(object))[i]); }
		virtual ConstString getClassName(void* object){ return makeTypeReflection<t_DataType>().getClassName(); }
		virtual EMetaType getMetaType(void* object){ return EMetaType_FixedArray; }
		virtual TypeId getTypeId(void* object) { return getTypeIdOf<FixedArray_t>(); }
	};
};

template<typename t_DataType, uint32_t t_capacity>
class alignas(t_DataType) LimitedArray {
private:
	uint8_t _arr[t_capacity * sizeof (t_DataType)];
	uint32_t _size = 0;
public:
	typedef LimitedArray<t_DataType, t_capacity> LimitedArray_t;
	typedef t_DataType DataType_t;
	static const uint32_t capacity = t_capacity;

	DataType_t& operator [] (int i) { return reinterpret_cast<t_DataType*>(_arr)[i]; }
	const DataType_t& operator [] (int i)const { return reinterpret_cast<t_DataType*>(_arr)[i]; }
	DataType_t& at(int i) { return reinterpret_cast<t_DataType*>(_arr)[i]; }
	const DataType_t& at(int i)const { return reinterpret_cast<t_DataType*>(_arr)[i]; }

	const t_DataType* begin()const { return reinterpret_cast<t_DataType*>(_arr); }
	const t_DataType* end()const { return reinterpret_cast<t_DataType*>(_arr) + _size; }
	t_DataType* begin() { return reinterpret_cast<t_DataType*>(_arr); }
	t_DataType* end() { return reinterpret_cast<t_DataType*>(_arr) + _size; }
	uint32_t size()const { return _size; }
	bool isFull(){ return _size == capacity; }
	bool isEmpty(){ return _size == 0; }

	void clear() {
		for (t_DataType* p = reinterpret_cast<t_DataType*>(_arr); p < end(); ++p)
			p->~t_DataType();
		_size = 0;
	}

	t_DataType& append(const t_DataType& v) {
		new (&at(_size)) t_DataType(v);
		return at(_size++);
	}

	t_DataType& append() {
		new (&at(_size)) t_DataType();
		return at(_size++);
	}

	t_DataType& insert(uint32_t iIndex) {
		if (size() > 0) {
			new (&at(_size)) t_DataType(at(_size - 1));
			at(_size - 1).~t_DataType();
			for (int i = _size - 2; i >= (int)iIndex; --i) {
				at(i + 1) = at(i);
				at(i).~t_DataType();
			}
			_size += 1;
			new (&at(iIndex)) t_DataType();
			return at(iIndex);
		} else {
			return append();
		}
	}

	void remove(uint32_t iIndex) {
		at(iIndex).~t_DataType();
		for (int i = iIndex + 1; i < (int)_size; ++i) {
			at(i - 1) = at(i);
		}
		_size -= 1;
	}


	void resize(uint32_t newSize) {
		if (newSize >= _size) {
			for (int i = _size; i < (int)newSize; ++i)
				new (&at(i)) t_DataType();
		} else {
			for (int i = newSize; i < (int)_size; ++i)
				at(i).~t_DataType();
		}
		_size = newSize;
	}


	class Reflector : public IReflector {
	public:
		virtual uint32_t getCapacity(void* object) { return LimitedArray_t::capacity; }
		virtual uint32_t getFieldsCount(void* object) { return reinterpret_cast<LimitedArray_t*>(object)->size(); }
		virtual ConstString getFieldName(void* object, uint32_t i) { return ConstString(); }
		virtual Reflection getFieldByName(void* object, ConstString name) { return Reflection(); }
		virtual Reflection getFieldByOffset(void* object, uint32_t offset) { return makeReflection((*reinterpret_cast<LimitedArray_t*>(object))[offset / sizeof(t_DataType)]); }
		virtual Reflection getFieldByNumber(void* object, uint32_t i) { return makeReflection((*reinterpret_cast<LimitedArray_t*>(object))[i]); }
		virtual ConstString getClassName(void* object){ return makeTypeReflection<t_DataType>().getClassName(); }
		virtual EMetaType getMetaType(void* object){ return EMetaType_LimitedArray; }
		virtual TypeId getTypeId(void* object) { return getTypeIdOf<LimitedArray_t>(); }
		virtual bool remove(void* object, uint32_t i) {
			LimitedArray_t& obj = *reinterpret_cast<LimitedArray_t*>(object);
			if (i >= obj.size())return false;
			obj.remove(i);
			return true;
		}
		virtual bool insert(void* object, uint32_t i) {
			LimitedArray_t& obj = *reinterpret_cast<LimitedArray_t*>(object);
			if (obj.isFull() || i > obj.size())return false;
			obj.insert(i);
			return true;
		}
		virtual bool append(void* object) {
			LimitedArray_t& obj = *reinterpret_cast<LimitedArray_t*>(object);
			if (obj.isFull())return false;
			obj.append();
			return true;
		}
		virtual bool clear(void* object){ reinterpret_cast<LimitedArray_t*>(object)->clear(); return true; }
	};
};

static_assert (alignof(MIR::LimitedArray<uint8_t, 1>) == alignof(uint32_t));


}
