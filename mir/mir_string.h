#pragma once

#include "mir_base.h"

#define MIR_ConstString(str) MIR::ConstString(str, sizeof(str) - 1)

namespace MIR {

template<uint32_t t_capacity>
class StringBuff {
private:
	char _arr[t_capacity + 1] = {0}; //null terminated
	uint32_t _size = 0;
public:
	static const uint32_t capacity = t_capacity;

	StringBuff(){}
	StringBuff(const char* s) {
		_size = min(strlen(s), t_capacity);
		memcpy(_arr, s, _size);
	}
	void operator = (const char* s) {
		_size = min(strlen(s), t_capacity);
		memcpy(_arr, s, _size);
	}
	inline StringBuff(class ConstString s);
	inline void operator = (class ConstString s);
	inline ConstString proxy();

	const char* begin()const { return _arr; }
	const char* end()const { return _arr + _size; }
	char* begin() { return _arr; }
	char* end() { return _arr + _size; }
	uint size()const { return _size; }
	bool isFull(){ return _size == capacity; }

	void clear() { _size = 0; }
	void resize(uint32_t s) { _size = min(s, capacity); }
	bool append(char c) {
		if (_size >= capacity)return false;
		_arr[_size ++] = c;
		return true;
	}
	bool append(const char* s) {
		int len = strlen(s);
		int copyLen = min(len, capacity - _size);
		memcpy(end(), s, copyLen);
		_size += copyLen;
		return copyLen == len;
	}
	inline bool append(ConstString s);

	char* c_str() {
		_arr[_size] = 0;
		return _arr;
	}

	bool parse(int32_t& n) { return sscanf(c_str(), "%d", &n) == 1; }
	bool parse(uint32_t& n) { return sscanf(c_str(), "%u", &n) == 1; }
	bool parse(int16_t& n) { int32_t cp; if (parse(cp)) { n = cp; return true; } return false; }
	bool parse(uint16_t& n) { uint32_t cp; if (parse(cp)) { n = cp; return true; } return false; }
	bool parse(int8_t& n) { int32_t cp; if (parse(cp)) { n = cp; return true; } return false; }
	bool parse(uint8_t& n) { uint32_t cp; if (parse(cp)) { n = cp; return true; } return false; }
	bool parse(float& f) { return sscanf(c_str(), "%f", &f) == 1; }
	inline bool parse(bool& b);

	//not safe(
	void print(int32_t n) { _size += sprintf(end(), "%d", (int)n); }
	void print(uint32_t n) { _size += sprintf(end(), "%u", (unsigned int)n); }
	void print(int16_t n) { _size += sprintf(end(), "%d", (int)n); }
	void print(uint16_t n) { _size += sprintf(end(), "%u", (unsigned int)n); }
	void print(int8_t n) { _size += sprintf(end(), "%d", (int)n); }
	void print(uint8_t n) { _size += sprintf(end(), "%u", (unsigned int)n); }
	void print(float f) { _size += sprintf(end(), "%f", f); }
	inline void print(bool b);

	void print(const char* str, ...) {
		va_list args;
		va_start(args, str);
		_size += vsprintf(end(), str, args);
		va_end(args);
	}
};

typedef StringBuff<27> ValueFormatStringBuff;
extern bool scan_value(ValueFormatStringBuff& buff, IReader& reader);

class ConstString {
private:
	const char* _begin;
	const char* _end;

public:
	ConstString() {
		_begin = nullptr;
		_end = nullptr;
	}

	ConstString(const char* begin, const char* end) {
		this->_begin = begin;
		this->_end = end;
	}

	ConstString(const char* str, uint lenght) {
		this->_begin = str;
		this->_end = _begin + lenght;
	}

	ConstString(const char* str) {
		this->_begin = str;
		this->_end = _begin + strlen(str);
	}

	template<uint32_t t_capacity>
	ConstString(const StringBuff<t_capacity>& b) {
		this->_begin = b.begin();
		this->_end = b.end();
	}

	const char* begin()const { return _begin; }
	const char* end()const { return _end; }
	uint size()const { return _end - _begin; }

	bool startsWith(ConstString prefix)const {
		uint lenpre = prefix.size();
		uint lenstr = size();
		return lenstr < lenpre ? false : strncmp(prefix.begin(), begin(), lenpre) == 0;
	}

	bool endsWith(ConstString postfix)const {
		uint lenpost = postfix.size();
		uint lenstr = size();
		return lenstr < lenpost ? false : strncmp(postfix.begin(), end() - lenpost, lenpost) == 0;
	}

	bool startsWith(char c)const {
		return size() > 0 && *begin() == c;
	}

	bool endsWith(char c)const {
		return size() > 0 && *(end() - 1) == c;
	}

	const char* find(char v)const {
		for (const char *c = begin(); c != end(); ++c)
			if (*c == v)
				return c;
		return nullptr;
	}

	const char* rfind(char v)const {
		for (const char *c = end() - 1; c >= begin(); --c)
			if (*c == v)
				return c;
		return nullptr;
	}

	ConstString trimSpaces()const {
		const char* c = begin();
		const char* e = end() - 1;
		while (c <= e) {
			if (isspace(*c)) {
				c ++;
			} else break;
		}
		while (c <= e) {
			if (isspace(*e)) {
				e --;
			} else break;
		}
		return ConstString(c, e + 1);
	}

	ConstString trimEnd(int n) {
		const char* e = end() - n;
		if (e >= this->_begin)
			return ConstString(this->_begin, e);
		else
			return ConstString(this->_begin, this->_begin);
	}

	friend bool operator == (const ConstString& A, const ConstString& B) {
		if (A.size() != B.size())
			return false;

		for (const char *a = A._begin, *b = B._begin, *a_e = A._end; a < a_e; ++a, ++b)
			if (*a != *b)
				return false;
		return true;
	}

	/*
	bool to(String63& dst) {
		uint l = size();
		if (l <= 63) {
			memcpy(dst.c, begin(), l);
			memset(dst.c + l, 0, l - 64);
			return true;
		}
		return false;
	}

	bool toNTS(char* buff, int buff_size) {
		int s = min(buff_size - 1, lenght());
		memcpy(buff, begin(), s);
		buff[s] = 0;
		return buff_size >= lenght() + 1;
	}

	bool appendNTS(char* buff, int buff_size) {
		int buffStrLen = strlen(buff);
		int newSize = buff_size - buffStrLen;
		if (newSize < 0)return false;
		return toNTS(buff + buffStrLen, newSize);
	}
	*/
	bool parse(int32_t& n);
	bool parse(uint32_t& n);
	bool parse(int16_t& n);
	bool parse(uint16_t& n);
	bool parse(int8_t& n);
	bool parse(uint8_t& n);
	bool parse(float& f);
	bool parse(bool& b);
};

template<uint32_t t_capacity>
StringBuff<t_capacity>::StringBuff(ConstString s) {
	_size = min(s.size(), capacity);
	memcpy(begin(), s.begin(), _size);
}

template<uint32_t t_capacity>
void StringBuff<t_capacity>::operator = (ConstString s) {
	_size = min(s.size(), capacity);
	memcpy(begin(), s.begin(), _size);
}

template<uint32_t t_capacity>
inline ConstString StringBuff<t_capacity>::proxy() {
	return ConstString(begin(), end());
}

template<uint32_t t_capacity>
inline bool StringBuff<t_capacity>::append(ConstString s) {
	auto copyLen = min(s.size(), capacity - _size);
	memcpy(end(), s.begin(), copyLen);
	_size += copyLen;
	return copyLen == s.size();
}

class ConstStringReader: public IReader {
public:
	ConstString str;
	bool endR = false;
	ConstStringReader(ConstString s) {
		str = s;
	}
	virtual bool read_impl(char* ch) {
		if (str.size() > 0) {
			*ch = *str.begin();
			str = ConstString(str.begin() + 1, str.end());
			return true;
		} else if (!endR){
			*ch = 0;
			endR = true;
			return true;
		} else
			return false;
	}
};

template<uint32_t t_capacity>
inline bool StringBuff<t_capacity>::parse(bool& b) {
	ConstString trimed = proxy().trimSpaces();
	if (trimed == "true") {
		b = true;
		return true;
	} else if (trimed == "false") {
		b = false;
		return true;
	} else {
		int32_t d;
		if (!parse(d))return false;
		b = d != 0;
		return true;
	}
}

template<uint32_t t_capacity>
inline void StringBuff<t_capacity>::print(bool b) {
	if (b)
		this->append(MIR_ConstString("true"));
	else
		this->append(MIR_ConstString("false"));
}

}
