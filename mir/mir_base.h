#pragma once

#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <new>
#include <math.h>

namespace MIR {

class FormattingParams {
public:
	uint32_t tabs = 0;
	bool singleLine = false;
	bool ignoreUnknownFields = true;
};

enum EMetaType : uint32_t {
	EMetaType_Unknown = 0,
	EMetaType_Value,
	EMetaType_Class,
	EMetaType_FixedArray,
	EMetaType_LimitedArray
};

class IWriter {
public:
	virtual bool write(const void* p, uint32_t size)=0;
	virtual bool writeLn() { return write("\n\r"); }
	bool writeTabs(int n);
	bool write(const char* srt);
	bool write(class ConstString srt);
	//bool print(const char* str, ...);
};

class IReader {
private:
	char previewChar;
	bool isPreviewChar = false;
public:
	virtual bool read_impl(char* ch)=0;

	bool read(char* ch) {
		if (isPreviewChar) {
			*ch = previewChar;
			isPreviewChar = false;
			return true;
		}
		return read_impl(ch);
	}

	bool preview(char* ch) {
		if (isPreviewChar) {
			*ch = previewChar;
			return true;
		}
		if (!read_impl(ch))return false;
		previewChar = *ch;
		isPreviewChar = true;
		return true;
	}

	bool readFirstNoSpace(char* c) {
		do {
			if (!read(c))return false;
		} while(isspace(*c));
		return true;
	}

	bool previewFirstNoSpace(char* c) {
		while(true) {
			if (!preview(c))return false;
			if (isspace(*c)) {
				if (!read(c))return false;
			} else break;
		}
		return true;
	}
};

static inline int min(int a, int b) { return a < b ? a : b; }

}

