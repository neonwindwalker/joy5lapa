#include "mir_reflections.h"
#include "mir_array.h"

namespace MIR {

/*
bool IWriter::print(const char* str, ...) {
	va_list args;
	va_start(args, str);
	char buff[128];
	vsprintf(buff, str, args);
	va_end(args);
	return write(buff);
}
*/

bool IWriter::writeTabs(int n) {
	for (int i = 0; i < n; ++i)
		if(!write("\t", 1))return false;
	return true;
}

bool IWriter::write(const char* srt) {
	return write(srt, strlen(srt));
}

bool IWriter::write(class ConstString srt) {
	return write(srt.begin(), srt.size());
}



bool ConstString::parse(int32_t& n) {
	if (size() > ValueFormatStringBuff::capacity)return false;
	return ValueFormatStringBuff(*this).parse(n);
}
bool ConstString::parse(uint32_t& n) {
	if (size() > ValueFormatStringBuff::capacity)return false;
	return ValueFormatStringBuff(*this).parse(n);
}
bool ConstString::parse(int16_t& n) {
	if (size() > ValueFormatStringBuff::capacity)return false;
	return ValueFormatStringBuff(*this).parse(n);
}
bool ConstString::parse(uint16_t& n) {
	if (size() > ValueFormatStringBuff::capacity)return false;
	return ValueFormatStringBuff(*this).parse(n);
}
bool ConstString::parse(int8_t& n) {
	if (size() > ValueFormatStringBuff::capacity)return false;
	return ValueFormatStringBuff(*this).parse(n);
}
bool ConstString::parse(uint8_t& n) {
	if (size() > ValueFormatStringBuff::capacity)return false;
	return ValueFormatStringBuff(*this).parse(n);
}
bool ConstString::parse(float& f) {
	if (size() > ValueFormatStringBuff::capacity)return false;
	return ValueFormatStringBuff(*this).parse(f);
}
bool ConstString::parse(bool& b) {
	if (size() > ValueFormatStringBuff::capacity)return false;
	return ValueFormatStringBuff(*this).parse(b);
}



bool IReflector::print(void* object, IWriter& writer, FormattingParams& fp) {
	EMetaType type = getMetaType(object);
	bool isArray = type == EMetaType_FixedArray || type == EMetaType_LimitedArray;

	int tabs = fp.tabs;
	bool first = true;

	if (isArray) {
		if (!writer.write("["))return false;
	} else {
		if (!writer.write("{"))return false;
		if (getPolymorphTypesCount(object) > 0) {
			if (!writer.write(" \"type\": \""))return false;
			if (!writer.write(getClassName(object)))return false;
			if (!writer.write("\""))return false;
			first = false;
		}
	}
	if (!fp.singleLine)
		writer.writeLn();
	tabs += 1;

	FormattingParams childFp = fp;
	childFp.tabs += 1;


	int fieldsCount = getFieldsCount(object);
	for (int nField = 0; nField < fieldsCount; ++nField) {
		Reflection r = getFieldByNumber(object, nField);
		bool rIsNotNull = r.isNotNull();

		if (!rIsNotNull && !isArray)
			continue;

		if (!first) {
			writer.write(",");
			if (!fp.singleLine)
				if (!writer.writeLn())return false;
		}

		if (!fp.singleLine)
			if (!writer.writeTabs(tabs))return false;
		if (!isArray) {
			if (!writer.write("\""))return false;
			if (!writer.write(getFieldName(object, nField)))return false;
			if (!writer.write(fp.singleLine ? "\":" : "\": "))return false;
		}

		if (rIsNotNull) {
			if (!r.print(writer, childFp))return false;
		} else {
			if (!writer.write("null"))return false;
		}

		first = false;
	}

	if (!first)
		if (!fp.singleLine)
			if (!writer.writeLn())return false;

	if (!fp.singleLine)
		if (!writer.writeTabs(fp.tabs))return false;
	if (isArray) {
		if (!writer.write("]"))return false;
	} else {
		if (!writer.write("}"))return false;
	}
	return true;
}

bool scanIgnore(IReader& reader) {
	int objCount = 0;
	int arrCount = 0;
	while(true) {
		char c;
		if (!reader.readFirstNoSpace(&c))return false;
		if (c == '"') {
			do {
				if (!reader.read(&c))return false;
			} while(c != '"');
		} else if (c == '{') {
			objCount ++;
		} else if (c == '}') {
			objCount --;
		} else if (c == '[') {
			arrCount ++;
		} else if (c == ']') {
			arrCount --;
		}
	} while(objCount != 0 || arrCount != 0);
	return true;
}

bool IReflector::scan(void* object, IReader& reader, FormattingParams& fp) {
	EMetaType type = getMetaType(object);
	char c;
	ValueFormatStringBuff name;
	if (type == EMetaType_Class) {
		if (!reader.readFirstNoSpace(&c))return false;
		if (c != '{')return false;
		while(true) {
			if (!reader.readFirstNoSpace(&c))return false;
			if (c == '}')break;

			//TODO " handling
			name.clear();
			if (isdigit(c))return false;

			if (c == '"') { //classic json "field_name":
				while (true) {
					if (!reader.read(&c))return false;
					if (c == '"')break;
					if (!name.append(c))return false;
				}
				if (!reader.readFirstNoSpace(&c))return false;
			} else { //alternative json field_name:
				while (isalnum(c)) {
					if (!name.append(c))return false;
					if (!reader.read(&c))return false;
				}
			}

			if (isspace(c))
				if (!reader.readFirstNoSpace(&c))return false;
			if (c != ':')return false;

			if (getPolymorphTypesCount(object) > 0 && name == MIR_ConstString("type")) {
				ValueFormatStringBuff type;
				if (!makeReflection(type).scan(reader, fp))return false;
				if (!polyNew(object, type))return false;
			} else {
				Reflection r = getFieldByName(object, name);
				if (r.isValid()) {
					if (!r.scan(reader, fp))return false;
				} else {
					if (fp.ignoreUnknownFields) {
						if (!scanIgnore(reader))return false;
					} else return false;
				}
			}

			if (!reader.readFirstNoSpace(&c))return false;
			if (c == '}')break;
			if (c != ',')return false;
		}
		return true;
	} else if (type == EMetaType_FixedArray || type == EMetaType_LimitedArray) {
		if (!reader.readFirstNoSpace(&c))return false;
		int index = 0;
		if (c != '[')return false;
		while(true) {
			if (!reader.previewFirstNoSpace(&c))return false;
			if (c == ']'){
				if (!reader.read(&c))return false;
				break;
			}

			if (type == EMetaType_LimitedArray && index >= getFieldsCount(object)) {
				append(object);
			}

			Reflection r = getFieldByNumber(object, index++);
			if (r.isValid()) {
				if (!r.scan(reader, fp))return false;
			} else {
				return false;
			}

			if (!reader.readFirstNoSpace(&c))return false;
			if (c == ']')break;
			if (c != ',')return false;
		}
		return true;
	}

	return false;
}

Reflection Reflection::getByPath(ConstString path) {
	Reflection r = *this;
	const char* nameStart = path.begin();
	for (const char* c = path.begin(); c < path.end(); ++c) {
		if (*c == '/' || c + 1 == path.end()) {
			ConstString name(nameStart, *c == '/' ? c : c + 1);
			if (name.size() == 0){
				nameStart = c + 1;
				continue;
			}
			EMetaType t = r.getMetaType();
			if (t == EMetaType_FixedArray || t == EMetaType_LimitedArray) {
				uint32_t index;
				if (!name.parse(index))
					return Reflection();
				r = r.getFieldByNumber(index);
			} else {
				r = r.getFieldByName(name);
			}
			nameStart = c + 1;
		}
	}
	return r;
}

bool scan_value(ValueFormatStringBuff& buff, IReader& reader) {
	buff.clear();

	char c;
	if (!reader.readFirstNoSpace(&c))return false;

	while (true) {
		if (!buff.append(c))return false;
		if (!reader.preview(&c))return false;
		if (isdigit(c) || c == '.' || c == '-') {
			if (!reader.read(&c))return false;
		} else break;
	}

	return true;
}

Reflection IReflector::getBaseClass(void* object, uint32_t i) { return Reflection(); }
Reflection IReflector::getPolymorphType(void* object, uint32_t n) { return Reflection(); }
Reflection InvalidReflection::getFieldByName(void* object, ConstString name){ return Reflection(); }
Reflection InvalidReflection::getFieldByNumber(void* object, uint32_t i){ return Reflection(); }

int gTypeIdSpinner = -1;

class MIRIniter {
public:
	MIRIniter() {
		registerTypeIdOf<int8_t>(1);
		registerTypeIdOf<uint8_t>(2);
		registerTypeIdOf<int16_t>(3);
		registerTypeIdOf<uint16_t>(4);
		registerTypeIdOf<int32_t>(5);
		registerTypeIdOf<uint32_t>(6);
		registerTypeIdOf<float>(7);
		registerTypeIdOf<bool>(8);
	}
};

MIRIniter gMIRIniter;

}
