#pragma once

#include "joy5_vector.h"

enum EKeyActionType {
	EKeyCodeType_KeyboardKey = 0,
	EKeyCodeType_SwitchLayout,
	EKeyCodeType_LikeShift
};

typedef uint32_t Joy5KeyCode;

inline EKeyActionType keyActionType(Joy5KeyCode kk){ return (EKeyActionType)(kk >> 28); }

class ActionKeyboardKey {
public:
	unsigned int code : 8;
	bool alt : 1;
	bool control : 1;
	bool shift : 1;
	bool global_like_shift_sensable : 1;
	unsigned int hw_layout : 2;

	unsigned int reserved : 14;

	EKeyActionType type : 4;

	ActionKeyboardKey(){
		static_assert(sizeof(ActionKeyboardKey) == sizeof(Joy5KeyCode));
		code = 0;
		alt = false;
		control = false;
		shift = false;
		global_like_shift_sensable = false;
		hw_layout = 0;
		reserved = 0;
		type = EKeyCodeType_KeyboardKey;
	}

	ActionKeyboardKey(Joy5KeyCode k){
		*reinterpret_cast<Joy5KeyCode*>(this) = k;
	}

	ActionKeyboardKey(uint8_t code, bool shift, bool global_like_shift_sensable = false) : ActionKeyboardKey() {
		this->code = code;
		this->shift = shift;
		this->global_like_shift_sensable = global_like_shift_sensable;
	}
	Joy5KeyCode u(){ return *reinterpret_cast<Joy5KeyCode*>(this); }
};

class ActionSwitchLayout {
public:
	unsigned int number : 8;
	unsigned int reserved : 20;
	EKeyActionType type : 4;

	ActionSwitchLayout(){
		static_assert(sizeof(ActionSwitchLayout) == sizeof(Joy5KeyCode));
		number = 0;
		reserved = 0;
		type = EKeyCodeType_SwitchLayout;
	}
	ActionSwitchLayout(Joy5KeyCode k) {
		*reinterpret_cast<Joy5KeyCode*>(this) = k;
	}
	Joy5KeyCode u(){ return *reinterpret_cast<Joy5KeyCode*>(this); }
};

class ActionLikeShift {
public:
	unsigned int reserved : 28;
	EKeyActionType type : 4;

	ActionLikeShift(){
		static_assert(sizeof(ActionLikeShift) == sizeof(Joy5KeyCode));
		reserved = 0;
		type = EKeyCodeType_LikeShift;
	}
	ActionLikeShift(Joy5KeyCode k) {
		*reinterpret_cast<Joy5KeyCode*>(this) = k;
	}
	Joy5KeyCode u(){ return *reinterpret_cast<Joy5KeyCode*>(this); }
};


