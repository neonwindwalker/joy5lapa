#pragma once

#include "joy5_actions.h"
#include "joy5_vector.h"

enum JoystickState {
	JoystickState_Await,

	//on any pin trigger
	JoystickState_LookingSector,

	//after applyDelayMS of keeping same pins
	JoystickState_PressedSector,

	//after longPressAfterMS in JoystickState_PressedSector state
	JoystickState_LongPressedSector,


	JoystickState_Leave
};

MICRO_DeclareData2(JoystickStatus, ,
		MIR::Nullable<uint32_t>, pressedSector,
		MIR::Nullable<uint32_t>, activeSwitchLayoutButtonParentLayout)
	}
};

MICRO_DeclareData2(Joystick, ,
		JoystickStatus, state,
		MIR::Nullable<uint32_t>, applyDelayMS)
	}

	uint32_t lastApplyTime = 0;
	int sector = -1;
	uint32_t sectorChangeTime = 0;

	void doAction(Joy5KeyCode kk);
	void doActionLeave(Joy5KeyCode kk);

	void tick(uint number, bool& printState);
};

