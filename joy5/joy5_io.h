#pragma once

#include "joy5_base.h"

static inline void my_print_str(const char* str) {
	joy5_send(str, strlen(str));
}

static inline void my_print_str(MIR::ConstString str) {
	joy5_send(str.begin(), str.size());
}

static inline void my_print(const char* str, ...) {
	va_list args;
	va_start(args, str);
	char buff[128];
	vsprintf(buff, str, args);
	my_print_str(buff);
	va_end(args);
}

static inline void my_print_ln() {
	my_print_str("\n\r");
}

static inline void my_print_begin_response_symbol() {
	char c = KEY_SOH;
	joy5_send(&c, 1);
}

static inline void my_print_end_response_symbol() {
	char c = KEY_EOT;
	joy5_send(&c, 1);
}

static inline void my_print_bell_begin_symbol() {
	char c = KEY_BEL;
	joy5_send(&c, 1);
}

static inline void my_print_bell_end_symbol() {
	char c = KEY_ETB;
	joy5_send(&c, 1);
}

class MyWriter: public MIR::IWriter {
public:
	virtual bool write(const void* p, uint32_t size) {
		my_print_str(MIR::ConstString((const char*)p, size));
		return true;
	}

	virtual bool writeLn() {
		my_print_ln();
		return true;
	}
};

class MyReader: public MIR::IReader {
public:
	bool echo = false;

	virtual bool read_impl(char* p) {
		while (joy5_receive(p, 1) != 1){
			joy5_delay();
		}
		if (echo) {
			joy5_send(p, 1);
		}
		return true;
	}
};

class MySettingsWriter: public MIR::IWriter {
public:
	virtual bool write(const void* p, uint32_t size) {
		const char* c = reinterpret_cast<const char*>(p);
		for (const char* c_e = c + size; c < c_e; ++c) {
			if (joy5_save_settings_string_char(*c) != 0)return false;
		}
		return true;
	}

	virtual bool writeLn() {
		write("\n\r", 2);
		return true;
	}
};

class MySettingsReader: public MIR::IReader {
public:
	virtual bool read_impl(char* p) {
		*p = joy5_load_settings_string_char();
		return *p != 0;
	}
};

