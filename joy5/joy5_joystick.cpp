#include "joy5.h"
#include "usb_hid_keys.h"

void Joystick::tick(uint number, bool& printState) {
	/*
	static uint8_t debug_colors[8][3] = {
				{0,0,100},
				{0,33,66},
				{0,66,33},
				{0,100,0},
				{33,66,0},
				{66,33,0},
				{100,0,0},
				{50,0,50}};
				*/
	static uint8_t debug_colors[8][3] = {
					{0, 100, 0},
					{0, 60, 50},
					{0, 0, 100},
					{50, 0, 50},
					{100, 100, 100},
					{100, 30, 30},
					{100, 0, 0},
					{50, 50, 0}};

	uint32_t tA = joy5_get_stick_pin_lastsettime(number, 0);
	uint32_t tB = joy5_get_stick_pin_lastsettime(number, 1);
	uint32_t tC = joy5_get_stick_pin_lastsettime(number, 2);
	uint32_t tD = joy5_get_stick_pin_lastsettime(number, 3);

	bool touchA = tA > lastApplyTime;
	bool touchB = tB > lastApplyTime;
	bool touchC = tC > lastApplyTime;
	bool touchD = tD > lastApplyTime;

	int prevSector = sector;
	if (touchA && !touchB && !touchC && !touchD) {
		sector = 2;
	} else if (!touchA && touchB && !touchC && !touchD) {
		sector = 4;
	} else if (!touchA && !touchB && touchC && !touchD) {
		sector = 6;
	} else if (!touchA && !touchB && !touchC && touchD) {
		sector = 0;
	} else if (touchA && touchB && !touchC && !touchD) {
		sector = 3;
	} else if (!touchA && touchB && touchC && !touchD) {
		sector = 5;
	} else if (!touchA && !touchB && touchC && touchD) {
		sector = 7;
	} else if (touchA && !touchB && !touchC && touchD) {
		sector = 1;
	} else if (!touchA && !touchB && !touchC && !touchD) {
		sector = -1;
	} else {
		//wrong state
		sector = -2;
	}

	uint32_t now = joy5_get_time();
	if (sector != prevSector) {
		sectorChangeTime = now;
	}

	uint32_t applyDelayValue = gJoy5.applyDelayMS;
	if (applyDelayMS.isNotNull()) {
		applyDelayValue = applyDelayMS.v();
	}

	uint32_t lastTouchTime = 0;
	if (touchA && lastTouchTime < tA)lastTouchTime = tA;
	if (touchB && lastTouchTime < tB)lastTouchTime = tB;
	if (touchC && lastTouchTime < tC)lastTouchTime = tC;
	if (touchD && lastTouchTime < tD)lastTouchTime = tD;
	bool bLeave = lastTouchTime + applyDelayValue <= now;
	bool bPress = !bLeave && sectorChangeTime + applyDelayValue <= now;

	if (number < joy5_get_leds_count() && joy5_is_can_set_leds()) {
		if (sector >= 0) {
			joy5_led_set(number, debug_colors[sector][0], debug_colors[sector][1], debug_colors[sector][2]);
		} else {
			joy5_led_set(number, 0, 0, 0);
		}
	}

	if (bPress) {

	} else {
		state.activeSwitchLayoutButtonParentLayout.deleteV();
		state.pressedSector.deleteV();
	}

	if (sectorChangeTime + applyDelayValue <= now) {
			if (state.entredSector.isNotNull()) {
				if (sector >= 0) {
					if (sector != state.entredSector.v()) {
						state.entredSector.v() = sector;
						printState = true;
					}
				} else {

					printState = true;
				}
			} else {
				if (sector >= 0) {
					state.entredSector.newV() = sector;
					printState = true;
				}
			}
		}


	if (sector >= -1 && !bLeave && !bPress)
		return;

	if (state.pushedButton.isNotNull()) {
		auto& bt = state.pushedButton.v();
		Joy5KeyCode kk = gJoy5.layouts[bt.layout][number][bt.sector];
		if (sector != bt.sector || bLeave) {
			state.pushedButton.deleteV();
			printState = true;
			switch(keyActionType(kk)) {
				case EKeyCodeType_SwitchLayout: {
					ActionSwitchLayout slkk(kk);
					for (int i = gJoy5.layoutsStack.size() - 1; i >= 0; --i) {
						if (gJoy5.layoutsStack[i] == slkk.number) {
							gJoy5.layoutsStack.resize(i);
							break;
						}
					}
				}
				break;
				case EKeyCodeType_LikeShift: {
					gJoy5.isLikeShift = false;
				}
				break;
				default: break;
			}
			lastApplyTime = now;
		}
		return;
	}

	if (sector < 0) {
		lastApplyTime = now;
		return;
	}

	auto layout = gJoy5.getCurrentLayout();
	auto kk = gJoy5.layouts[layout][number][sector];

	if (bPress) {
		switch(keyActionType(kk)) {
			case EKeyCodeType_SwitchLayout: {
				ActionSwitchLayout act(kk);
				auto& bt = state.pushedButton.newV();
				bt.layout = layout;
				bt.sector = sector;
				gJoy5.layoutsStack.append(act.number);
				lastApplyTime = now;
				printState = true;
				return;
			}
			case EKeyCodeType_LikeShift: {
				gJoy5.isLikeShift = true;
				lastApplyTime = now;
				printState = true;
				return;
			}
			default: break;
		}
	}

	if (bLeave && keyActionType(kk) == EKeyCodeType_KeyboardKey) {
		ActionKeyboardKey act(kk);
		joy5_send_keyboard_keycode(act.hw_layout, act.code,
				false,
				false,
				act.global_like_shift_sensable ? gJoy5.isLikeShift : act.shift);
		lastApplyTime = now;
		return;
	}
}
