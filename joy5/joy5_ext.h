#pragma once

#ifdef __cplusplus
	#define JOY5_EXTERN_C extern "C"
#else
	#define JOY5_EXTERN_C extern
#endif

#define JOY5_STICKS 5
#define JOY5_STICK_PINS 4
typedef int Joy5ButtonStatus;
typedef int Joy5Boolean;
typedef int Joy5ErrorCode;
#define JOY5_BUTTON_ON -1
#define JOY5_BUTTON_OFF 0
#define JOY5_BUTTON_UNDEFINED 0xAAAAAAAA

JOY5_EXTERN_C void joy5_init();

JOY5_EXTERN_C int joy5_get_stick_pin_lastsettime(int stick, int pin);
JOY5_EXTERN_C Joy5ButtonStatus joy5_get_button_help();
JOY5_EXTERN_C Joy5ButtonStatus joy5_get_button_switch_layout();
JOY5_EXTERN_C Joy5ButtonStatus joy5_get_button_game();

JOY5_EXTERN_C void joy5_send_keyboard_keycode(int device, int keycode, Joy5Boolean is_alt, Joy5Boolean is_control, Joy5Boolean is_shift);

JOY5_EXTERN_C int joy5_get_time();
JOY5_EXTERN_C void joy5_delay();
JOY5_EXTERN_C void joy5_tick();

JOY5_EXTERN_C Joy5ErrorCode joy5_send(const char* data, int data_len);
JOY5_EXTERN_C Joy5Boolean joy5_is_send_buffer_empty();
JOY5_EXTERN_C int joy5_receive(char* data, int data_len);//negative on error

JOY5_EXTERN_C Joy5Boolean joy5_begin_load_settings();
JOY5_EXTERN_C char joy5_load_settings_string_char(); //null terminated string
JOY5_EXTERN_C void joy5_end_load_settings();
JOY5_EXTERN_C Joy5Boolean joy5_begin_save_settings();
JOY5_EXTERN_C Joy5Boolean joy5_save_settings_string_char(char c);
JOY5_EXTERN_C Joy5Boolean joy5_end_save_settings();

JOY5_EXTERN_C Joy5Boolean joy5_is_can_set_leds();
JOY5_EXTERN_C Joy5Boolean joy5_set_leds_count(int count);
JOY5_EXTERN_C int joy5_get_leds_count();
JOY5_EXTERN_C void joy5_led_set(int index, uint8_t r, uint8_t g, uint8_t b);


