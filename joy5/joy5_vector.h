#pragma once

#include "joy5_base.h"

MICRO_DeclareData2(Vector2f, ,
	float, x,
	float, y)
		x = 0.0f;
		y = 0.0f;
	}

	Vector2f(float srcX, float srcY) :  x(srcX), y(srcY)	{}
	Vector2f(const float* arr) : x(arr[0]), y(arr[1])		{}
	Vector2f(const Vector2f& u) : x(u.x), y(u.y)			{}

	float* a() 												{ return reinterpret_cast<float*>(this); }
	const float* a()const 									{ return reinterpret_cast<const float*>(this); }
	float& operator [] (int index)							{ return this->a()[index]; }
	const float& operator [] (int index) const				{ return this->a()[index]; }
	Vector2f& operator = (const Vector2f& u)				{ x = u.x; y = u.y; return *this; }
	bool operator == (const Vector2f& u)const				{ return x == u.x && y == u.y; }
	bool operator != (const Vector2f& u)const				{ return x != u.x || y != u.y; }
	Vector2f operator + () const							{ return *this; }
	Vector2f operator - () const							{ return Vector2f(-x, -y); }
	
	void operator += (const Vector2f& u)					{ x += u.x; y += u.y; }
	void operator -= (const Vector2f& u)					{ x -= u.x; y -= u.y; }
	void operator += (const float f)						{ x += f; y += f; }
	void operator -= (const float f)						{ x -= f; y -= f; }
	void operator *= (const Vector2f& u)					{ x *= u.x; y *= u.y; }
	void operator /= (const Vector2f& u)					{ x /= u.x; y /= u.y; }
	void operator *= (const float f)						{ x *= f; y *= f; }
	void operator /= (const float f)						{ x /= f; y /= f; }

	friend  Vector2f operator + (const Vector2f& a, const Vector2f& b){ return Vector2f(a.x + b.x, a.y + b.y); }
	friend  Vector2f operator - (const Vector2f& a, const Vector2f& b){ return Vector2f(a.x - b.x, a.y - b.y); }
	friend  Vector2f operator * (const Vector2f& a, const Vector2f& b){ return Vector2f(a.x * b.x, a.y * b.y); }
	friend  Vector2f operator / (const Vector2f& a, const Vector2f& b){ return Vector2f(a.x / b.x, a.y / b.y); }
	friend  Vector2f operator + (const float f, const Vector2f& v) { return Vector2f(f + v.x, f + v.y); }
	friend  Vector2f operator - (const float f, const Vector2f& v) { return Vector2f(f - v.x, f - v.y); }
	friend  Vector2f operator * (const float f, const Vector2f& v) { return Vector2f(f * v.x, f * v.y); }
	friend  Vector2f operator / (const float f, const Vector2f& v) { return Vector2f(f / v.x, f / v.y); }
	friend  Vector2f operator + (const Vector2f& v, const float f) { return Vector2f(v.x + f, v.y + f); }
	friend  Vector2f operator - (const Vector2f& v, const float f) { return Vector2f(v.x - f, v.y - f); }
	friend  Vector2f operator * (const Vector2f& v, const float f) { return Vector2f(v.x * f, v.y * f); }
	friend  Vector2f operator / (const Vector2f& v, const float f) { return Vector2f(v.x / f, v.y / f); }
	
	float lenghtSquare()const									{ return x * x + y * y; }
	float lenght()const											{ return sqrtf(lenghtSquare()); }
	float distanceSquareTo(const Vector2f& u)const				{ return (u - *this).lenghtSquare(); }
	float distanceTo(const Vector2f& u)const					{ return (u - *this).lenght(); }
	Vector2f normal()const										{ return *this / lenght(); }
	
	static Vector2f getZero() 									{ return Vector2f(float(0), float(0)); }
	static Vector2f getOne() 									{ return Vector2f(float(1), float(1)); }
};

static inline float dot(const Vector2f& u, const Vector2f& v)			{ return u.x * v.x + u.y * v.y; }


