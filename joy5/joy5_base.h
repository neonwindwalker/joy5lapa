#pragma once

#include <math.h>

#include "../mir/mir.h"
#include "../microrl/microrl.h"
#undef true
#undef false

#include "joy5_ext.h"


#define JOY5_MAX_STICK_SECTORS 10
#define JOY5_MAX_LAYOUTS 20


typedef MIR::StringBuff<44> CommonString;

typedef unsigned int uint;

static inline float radiansTo0_2pi(float rad) {
	if (rad >= 0.0f)
		return fmod(rad, float(M_PI * 2));
	else
		return float(M_PI * 2) - fmod(-rad, float(M_PI * 2));
}

static inline float radiansAngleToInterpolatePoints(float rad, uint size, uint& lowIndex, uint& hiIndex) {
	float radN = radiansTo0_2pi(rad) / float(M_PI * 2) * float(size);
	lowIndex = uint(radN);
	hiIndex = lowIndex + 1;
	if (hiIndex >= size)
		hiIndex = 0;
	return radN - (float)lowIndex;
}

static inline uint radiansAngleToBestPoint(float rad, uint size) {
	return uint(radiansTo0_2pi(rad) / float(M_PI * 2) * float(size) + 0.5f) % size;
}
