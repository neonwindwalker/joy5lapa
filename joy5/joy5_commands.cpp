#include "joy5.h"
#include "usb_hid_keys.h"

extern microrl_t gMicroRL;

void cmd_help_exec(int argc, const char* const* argv) {
	my_print_str("list of all commands:");
	my_print_ln();
	my_print_str(JOY5_CMD_HELP); my_print_ln();
	my_print_str(JOY5_CMD_WATCH_SENSORS); my_print_ln();
	my_print_str(JOY5_CMD_WATCH_STATE); my_print_ln();
	my_print_str(JOY5_CMD_SAVE_ALL_SETTINGS_TO_FLASH); my_print_ln();
	my_print_str(JOY5_CMD_LS); my_print_ln();
	my_print_str(JOY5_CMD_CD); my_print_ln();
	my_print_str(JOY5_CMD_PWD); my_print_ln();
	my_print_str(JOY5_CMD_PRINT); my_print_ln();
	my_print_str(JOY5_CMD_SCAN); my_print_ln();
	my_print_str(JOY5_CMD_SET); my_print_ln();
	my_print_str(JOY5_CMD_RESET); my_print_ln();
}

void my_print_error() {
	my_print_str("error");
}

bool save_settings() {
	int err = joy5_begin_save_settings();
	if (err != 0)return false;

	MySettingsWriter writer;
	MIR::FormattingParams fp;
	if (!MIR::makeReflection(gJoy5).print(writer, fp))
		return false;

	return joy5_end_save_settings();
}

int my_rl_exec(int argc, const char* const* argv) {
	MIR::ConstString cmdname = argv[0];

	my_print_ln();
	my_print_begin_response_symbol();

	if (cmdname == JOY5_CMD_HELP) {
		cmd_help_exec(argc - 1, argv + 1);
	} else if (cmdname == JOY5_CMD_ABOUT) {
		DeviceAbout data;
		MIR::FormattingParams fp;
		if (!MIR::makeReflection(data).print(gJoy5.writer, fp))
			my_print_error();
	}  else if (cmdname == JOY5_CMD_CD) {
		if (!MIR::console_cmd_cd_exec(MIR::makeReflection(gJoy5), gJoy5.workDir, argc - 1, argv + 1))
			my_print_error();
	} else if (cmdname == JOY5_CMD_LS) {
		if (!MIR::console_cmd_ls_exec(gJoy5.writer, MIR::makeReflection(gJoy5), gJoy5.workDir, argc - 1, argv + 1))
			my_print_error();
	} else if (cmdname == JOY5_CMD_PWD) {
		my_print_str(gJoy5.workDir);
	} else if (cmdname == JOY5_CMD_PRINT) {
		MIR::FormattingParams fp;
		if (!MIR::console_cmd_print_exec(gJoy5.writer, MIR::makeReflection(gJoy5), gJoy5.workDir, fp, argc - 1, argv + 1))
			my_print_error();
	} else if (cmdname == JOY5_CMD_SET) {
		if (!MIR::console_cmd_set_exec(MIR::makeReflection(gJoy5), gJoy5.workDir, argc - 1, argv + 1))
			my_print_error();
	} else if (cmdname == JOY5_CMD_SCAN) {
		MIR::FormattingParams fp;
		gJoy5.reader.echo = true;
		if (!MIR::console_cmd_scan_exec(gJoy5.reader, MIR::makeReflection(gJoy5), gJoy5.workDir, fp, argc - 1, argv + 1))
			my_print_error();
		gJoy5.reader.echo = false;
	} else if (cmdname == JOY5_CMD_WATCH_STATE || cmdname == JOY5_CMD_WATCH_SENSORS) {
		if (argc > 1) {
			MIR::ConstString countStr(argv[1]);
			int32_t count;
			if (countStr.parse(count)) {
				gJoy5.activeCommandNeedSensCount = count;
			} else {
				gJoy5.activeCommandNeedSensCount = 0;
			}
		} else gJoy5.activeCommandNeedSensCount = 0;
		if (cmdname == JOY5_CMD_WATCH_STATE) {
			if (gJoy5.activeCommandNeedSensCount != 0)
				gJoy5.activeCommand = Joy5ActiveCommand_WatchState;
			print_watch_state();
		} else {
			if (gJoy5.activeCommandNeedSensCount != 0)
				gJoy5.activeCommand = Joy5ActiveCommand_WatchSensors;
			print_watch_sensors();
		}
	} else if (cmdname == JOY5_CMD_SAVE_ALL_SETTINGS_TO_FLASH) {
		if (!save_settings())
			my_print_error();
	} else if (cmdname == JOY5_CMD_RESET) {
		gJoy5.resetEntrys();
	} else {
		my_print("unknown command");
		return -1;
	}
	my_print_end_response_symbol();
	my_print_ln();
	return 0;
}


const char* gCompletionNames[32];
int gCompletionNamesCount;

void addToCompletionNames(const char* name, const char* cmdFullName) {
	if (*name == 0)
		return;
	const char* cmd = cmdFullName;
	while(*name != 0 && *cmd != 0) {
		if (*name != *cmd)
			return;
		name ++;
		cmd ++;
	}
	gCompletionNames[gCompletionNamesCount++] = cmdFullName;
}

char ** my_rl_completion(int argc, const char* const* argv) {
	gCompletionNamesCount = 0;
	if (argc >= 1) {
		const char* name = argv[0];
		addToCompletionNames(name, JOY5_CMD_HELP);
		addToCompletionNames(name, JOY5_CMD_WATCH_SENSORS);
		addToCompletionNames(name, JOY5_CMD_WATCH_STATE);
		addToCompletionNames(name, JOY5_CMD_SAVE_ALL_SETTINGS_TO_FLASH);
	}
	gCompletionNames[gCompletionNamesCount] = 0;
	return (char**)gCompletionNames;
}

void my_rl_sigint() {
	//dot response my_print_begin_response_symbol/my_print_end_response_symbol
	my_print_ln();
	gJoy5.activeCommand = Joy5ActiveCommand_None;
	microrl_insert_char (&gMicroRL, KEY_DC2); //?
}

void print_watch_state() {
	WatchStateData dat;
	MIR::FormattingParams fp;
	fp.singleLine = true;
	MIR::makeReflection(dat).print(gJoy5.writer, fp);
}

void print_watch_sensors() {
	WatchSensorsData dat;
	MIR::FormattingParams fp;
	fp.singleLine = true;
	MIR::makeReflection(dat).print(gJoy5.writer, fp);
}

void joy5_init() {
	microrl_init (&gMicroRL, &my_print_str);
	microrl_set_execute_callback (&gMicroRL, my_rl_exec);
	microrl_set_complete_callback (&gMicroRL, my_rl_completion);
	microrl_set_sigint_callback (&gMicroRL, my_rl_sigint);

	joy5_begin_load_settings();

	MySettingsReader reader;
	MIR::FormattingParams fp;
	MIR::makeReflection(gJoy5).scan(reader, fp);

	joy5_end_load_settings();

	if (gJoy5.check())
		save_settings();

	/*
	joy5_begin_load_settings();

	MySettingsReader reader2;
	MIR::FormattingParams fp2;
	Joy5 j2;
	MIR::makeReflection(j2).scan(reader2, fp2);

	joy5_end_load_settings();
	*/
}
