#include "joy5.h"

#include "joy5_joystick.h"

microrl_t gMicroRL;
Joy5 gJoy5;

extern "C" void joy5_tick() {
	gJoy5.tick();
}

void Joy5::printState() {
	my_print_bell_begin_symbol();
	my_print("state:");
	print_watch_state();
	my_print_bell_end_symbol();
}

void Joy5::tick() {
	if (activeCommand == Joy5ActiveCommand_WatchSensors) {
		if (joy5_is_send_buffer_empty()) {
			my_print("\r");
			my_print_bell_begin_symbol();
			my_print("sensors:");
			print_watch_sensors();
			my_print_bell_end_symbol();
			activeCommandNeedSensCount --;
			if (activeCommandNeedSensCount == 0)
				activeCommand = Joy5ActiveCommand_None;
		}
	} else if (activeCommand == Joy5ActiveCommand_WatchState) {
		if (joy5_is_send_buffer_empty()) {
			my_print("\r");
			printState();
			activeCommandNeedSensCount --;
			if (activeCommandNeedSensCount == 0)
				activeCommand = Joy5ActiveCommand_None;
		}
	}

	char received;
	while (joy5_receive(&received, 1) == 1) {
		if (received != KEY_EOT) {
			activeCommand = Joy5ActiveCommand_None;
			activeCommandNeedSensCount = 0;
		}

		microrl_insert_char (&gMicroRL, received);
	}

	bool needPrintState = false;
	bool btNeedHelp = joy5_get_button_help();
	if (isNeedShowHelp != btNeedHelp) {
		isNeedShowHelp = btNeedHelp;
		needPrintState = true;
	}

	for (int nStick=0; nStick < JOY5_STICKS; ++nStick)
		joysticks[nStick].tick(nStick, needPrintState);

	if (needPrintState) {
		printState();
	}
}

bool Joy5::check() {
	bool needResave = false;
	if (layouts.isEmpty()) {
		resetEntrys();
		needResave = true;
	}
	return needResave;
}









