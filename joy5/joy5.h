#pragma once

#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>
#include "joy5_ext.h"
#include "joy5_io.h"
#include "joy5_actions.h"
#include "joy5_joystick.h"

typedef MIR::FixedArray<Joystick, JOY5_STICKS> JoysticksArray;
typedef MIR::LimitedArray<MIR::FixedArray<MIR::LimitedArray<Joy5KeyCode, JOY5_MAX_STICK_SECTORS>, JOY5_STICKS>,JOY5_MAX_LAYOUTS> LayoutArray3d;
typedef MIR::LimitedArray<uint8_t, 32> LayotsStackArray;

MICRO_DeclareData2(DeviceAbout, ,
	CommonString, device,
	uint32_t, fw_version)
		device = "Joy5";
		fw_version = 0;
	}
};

typedef enum
{
	Joy5ActiveCommand_None = 0,
	Joy5ActiveCommand_WatchSensors,
	Joy5ActiveCommand_WatchState
} Joy5ActiveCommand;


MICRO_DeclareData4(Joy5, ,
		JoysticksArray, joysticks,
		LayoutArray3d, layouts,
		uint32_t, applyDelayMS,
		uint32_t, longPressAfterMS)
		workDir = "/";
		applyDelayMS = 100;
		longPressAfterMS = 1000;
	}

	LayotsStackArray layoutsStack;
	bool isLikeShift = false;
	bool isNeedShowHelp = false;

	void tick();

	uint getCurrentLayout() {
		if (layoutsStack.size() == 0)return 0;
		return layoutsStack[layoutsStack.size() - 1];
	}

	MIR::StringBuff<256> workDir;

	Joy5ActiveCommand activeCommand = Joy5ActiveCommand_None;
	int activeCommandNeedSensCount = 0;
	MyWriter writer;
	MyReader reader;

	void printState();
	void resetEntrys();
	bool check();
};

extern Joy5 gJoy5;


#define JOY5_CMD_HELP "help"
#define JOY5_CMD_ABOUT "about"
#define JOY5_CMD_WATCH_SENSORS "watch_sensors"
#define JOY5_CMD_WATCH_STATE "watch_state"
#define JOY5_CMD_SAVE_ALL_SETTINGS_TO_FLASH "save_all_settings_to_flash"
#define JOY5_CMD_RESET "reset"

#define JOY5_CMD_LS "ls"
#define JOY5_CMD_CD "cd"
#define JOY5_CMD_PWD "pwd"
#define JOY5_CMD_PRINT "print"
#define JOY5_CMD_SET "set"
#define JOY5_CMD_SCAN "scan"

void cmd_help_exec(int argc, const char* const* argv);
int cmd_calibrate_exec(int argc, const char* const* argv);

void print_watch_state();
void print_watch_sensors();

typedef MIR::FixedArray<uint16_t, JOY5_STICKS> WatchSensorsArray;
typedef MIR::FixedArray<JoystickState, JOY5_STICKS> JoystickStateArray;

MICRO_DeclareData2(WatchSensorsData, ,
	WatchSensorsArray, sensors,
	uint32_t, buttons)
		uint32_t now = joy5_get_time();
		for (int i=0; i < JOY5_STICKS; ++i) {
			sensors[i] = (joy5_get_stick_pin_lastsettime(i, 0) + 100 > now ? 1 : 0) +
					(joy5_get_stick_pin_lastsettime(i, 1) + 100 > now ? 10 : 0) +
					(joy5_get_stick_pin_lastsettime(i, 2) + 100 > now ? 100 : 0) +
					(joy5_get_stick_pin_lastsettime(i, 3) + 100 > now ? 1000 : 0);
		}
		buttons = joy5_get_button_switch_layout() == JOY5_BUTTON_ON ? 1 : 0 +
				joy5_get_button_game() == JOY5_BUTTON_ON ? 10 : 0 +
						joy5_get_button_help() == JOY5_BUTTON_ON ? 100 : 0;
	}
};


MICRO_DeclareData3(WatchStateData, ,
	uint32_t, layout,
	bool, needShowHelp,
	JoystickStateArray, joysticks)
		for (int i=0; i < JOY5_STICKS; ++i) {
			joysticks[i] = gJoy5.joysticks[i].state;
		}
		layout = gJoy5.getCurrentLayout();
		needShowHelp = gJoy5.isNeedShowHelp;
	}
};



