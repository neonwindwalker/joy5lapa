#include "joy5.h"
#include "usb_hid_keys.h"

void Joy5::resetEntrys() {
	layouts.clear();
	layouts.resize(3);

	//alpha
	layouts[0][4].append(ActionKeyboardKey(USB_HID_KEY_A, false).u());
	layouts[0][4].append(ActionKeyboardKey(USB_HID_KEY_COMMA, false).u());
	layouts[0][4].append(ActionKeyboardKey(USB_HID_KEY_O, false).u());
	layouts[0][4].append(ActionKeyboardKey(USB_HID_KEY_I, false).u());
	layouts[0][4].append(ActionKeyboardKey(USB_HID_KEY_U, false).u());
	layouts[0][4].append(ActionKeyboardKey(USB_HID_KEY_Y, false).u());
	layouts[0][4].append(ActionKeyboardKey(USB_HID_KEY_ENTER, false).u());
	layouts[0][4].append(ActionKeyboardKey(USB_HID_KEY_ENTER, false).u());

	layouts[0][3].append(ActionKeyboardKey(USB_HID_KEY_B, false).u());
	layouts[0][3].append(ActionKeyboardKey(USB_HID_KEY_C, false).u());
	layouts[0][3].append(ActionKeyboardKey(USB_HID_KEY_D, false).u());
	layouts[0][3].append(ActionKeyboardKey(USB_HID_KEY_F, false).u());
	layouts[0][3].append(ActionKeyboardKey(USB_HID_KEY_G, false).u());
	layouts[0][3].append(ActionKeyboardKey(USB_HID_KEY_H, false).u());
	layouts[0][3].append(ActionKeyboardKey(USB_HID_KEY_J, false).u());
	layouts[0][3].append(ActionKeyboardKey(USB_HID_KEY_K, false).u());

	layouts[0][2].append(ActionKeyboardKey(USB_HID_KEY_L, false).u());
	layouts[0][2].append(ActionKeyboardKey(USB_HID_KEY_M, false).u());
	layouts[0][2].append(ActionKeyboardKey(USB_HID_KEY_N, false).u());
	layouts[0][2].append(ActionKeyboardKey(USB_HID_KEY_P, false).u());
	layouts[0][2].append(ActionKeyboardKey(USB_HID_KEY_Q, false).u());
	layouts[0][2].append(ActionKeyboardKey(USB_HID_KEY_R, false).u());
	layouts[0][2].append(ActionKeyboardKey(USB_HID_KEY_S, false).u());
	layouts[0][2].append(ActionKeyboardKey(USB_HID_KEY_T, false).u());


	layouts[0][0].append(ActionKeyboardKey(USB_HID_KEY_V, false).u());
	layouts[0][0].append(ActionKeyboardKey(USB_HID_KEY_W, false).u());
	layouts[0][0].append(ActionKeyboardKey(USB_HID_KEY_X, false).u());
	layouts[0][0].append(ActionKeyboardKey(USB_HID_KEY_Z, false).u());
	layouts[0][0].append(ActionKeyboardKey(USB_HID_KEY_SPACE, false).u());
	layouts[0][0].append(ActionKeyboardKey(USB_HID_KEY_SPACE, false).u());
	layouts[0][0].append(ActionKeyboardKey(USB_HID_KEY_SPACE, false).u());
	layouts[0][0].append(ActionKeyboardKey(USB_HID_KEY_SPACE, false).u());

	ActionLikeShift shiftAll;

	ActionSwitchLayout switchToNumLayout;
	switchToNumLayout.number = 1;

	ActionSwitchLayout switchToSymbolsLayout;
	switchToSymbolsLayout.number = 2;

	layouts[0][1].append(ActionKeyboardKey(USB_HID_KEY_SPACE, false).u());
	layouts[0][1].append(shiftAll.u());
	layouts[0][1].append(switchToSymbolsLayout.u());
	layouts[0][1].append(switchToNumLayout.u());

	//num
	layouts[1][4].append(ActionKeyboardKey(USB_HID_KEY_DOT, false).u());
	layouts[1][4].append(ActionKeyboardKey(USB_HID_KEY_COMMA, false).u());
	layouts[1][4].append(ActionKeyboardKey(USB_HID_KEY_SLASH, true).u());
	layouts[1][4].append(ActionKeyboardKey(USB_HID_KEY_1, true).u());
	layouts[1][4].append(ActionKeyboardKey(USB_HID_KEY_APOSTROPHE, false).u());
	layouts[1][4].append(ActionKeyboardKey(USB_HID_KEY_COMMA, true).u());
	layouts[1][4].append(ActionKeyboardKey(USB_HID_KEY_DOT, true).u());
	layouts[1][4].append(ActionKeyboardKey(USB_HID_KEY_EQUAL, false).u());

	layouts[1][3].append(ActionKeyboardKey(USB_HID_KEY_5, false).u());
	layouts[1][3].append(ActionKeyboardKey(USB_HID_KEY_6, false).u());
	layouts[1][3].append(ActionKeyboardKey(USB_HID_KEY_7, false).u());
	layouts[1][3].append(ActionKeyboardKey(USB_HID_KEY_8, false).u());
	layouts[1][3].append(ActionKeyboardKey(USB_HID_KEY_9, false).u());
	layouts[1][3].append(ActionKeyboardKey(USB_HID_KEY_8, true).u());
	layouts[1][3].append(ActionKeyboardKey(USB_HID_KEY_SLASH, false).u());
	layouts[1][3].append(ActionKeyboardKey(USB_HID_KEY_9, true).u());

	layouts[1][2].append(ActionKeyboardKey(USB_HID_KEY_0, false).u());
	layouts[1][2].append(ActionKeyboardKey(USB_HID_KEY_1, false).u());
	layouts[1][2].append(ActionKeyboardKey(USB_HID_KEY_2, false).u());
	layouts[1][2].append(ActionKeyboardKey(USB_HID_KEY_3, false).u());
	layouts[1][2].append(ActionKeyboardKey(USB_HID_KEY_4, false).u());
	layouts[1][2].append(ActionKeyboardKey(USB_HID_KEY_EQUAL, true).u());
	layouts[1][2].append(ActionKeyboardKey(USB_HID_KEY_MINUS, false).u());
	layouts[1][2].append(ActionKeyboardKey(USB_HID_KEY_0, true).u());

	layouts[1][0].append(ActionKeyboardKey(USB_HID_KEY_BACKSPACE, false).u());
	layouts[1][0].append(ActionKeyboardKey(USB_HID_KEY_ESC, false).u());
	layouts[1][0].append(ActionKeyboardKey(USB_HID_KEY_DELETE, false).u());
	layouts[1][0].append(ActionKeyboardKey(USB_HID_KEY_F1, false).u());
	layouts[1][0].append(ActionKeyboardKey(USB_HID_KEY_SPACE, false).u());
	layouts[1][0].append(ActionKeyboardKey(USB_HID_KEY_SPACE, false).u());
	layouts[1][0].append(ActionKeyboardKey(USB_HID_KEY_SPACE, false).u());
	layouts[1][0].append(ActionKeyboardKey(USB_HID_KEY_SPACE, false).u());

	//symbols
	layouts[2][4].append(ActionKeyboardKey(USB_HID_KEY_LEFTBRACE, false).u());
	layouts[2][4].append(ActionKeyboardKey(USB_HID_KEY_RIGHTBRACE, false).u());
	layouts[2][4].append(ActionKeyboardKey(USB_HID_KEY_LEFTBRACE, true).u());
	layouts[2][4].append(ActionKeyboardKey(USB_HID_KEY_RIGHTBRACE, true).u());
	layouts[2][4].append(ActionKeyboardKey(USB_HID_KEY_MINUS, true).u());
	layouts[2][4].append(ActionKeyboardKey(USB_HID_KEY_SPACE, false).u());
	layouts[2][4].append(ActionKeyboardKey(USB_HID_KEY_SPACE, false).u());
	layouts[2][4].append(ActionKeyboardKey(USB_HID_KEY_SPACE, false).u());
}

